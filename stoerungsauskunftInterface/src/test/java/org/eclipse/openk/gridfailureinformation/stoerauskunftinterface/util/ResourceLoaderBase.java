/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.util;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Log4j2
public class ResourceLoaderBase {
    private String stream2String(InputStream is, String filename) {
        StringWriter writer = new StringWriter();
        BOMInputStream bomInputStream = new BOMInputStream(is, false, ByteOrderMark.UTF_8, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_16LE,
            ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE);

        try {
            IOUtils.copy(bomInputStream, writer, StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            log.error("Fehler in stream2String()", e);
            return "";
        }

        log.debug("Datei erfolgreich eingelesen: " + filename);
        return writer.toString();
    }

    public String loadStringFromResource(String filename) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(filename);
        if (inputStream==null){
            log.error("Datei nicht gefunden: " + filename);
            return null;
        }
        log.debug("Datei existiert: " + filename);
        try {
            URL resource = classLoader.getResource(filename);
            if (resource != null) {
                URI uri = resource.toURI();
                log.debug("Uniform Resource Identifier (URI): " + uri);
            }
        } catch (URISyntaxException e) {
            log.error("Fehler in loadStringFromResource: " + filename, e);
        }

        return stream2String(inputStream, filename);
    }

    public String loadFromPath(String path) {
        try {
            Path paths = Paths.get(path);
            log.debug("paths: " + path);
            try (InputStream inputStream = Files.newInputStream(paths)) {
                return stream2String(inputStream, paths.getFileName().toString());
            }
        } catch (IOException e) {
            log.error("Fehler in loadFromPath", e);
            return null;
        }
    }
}
