package org.eclipse.openk.gridfailureinformation.viewmodel;

import lombok.Data;

import java.util.List;

@Data
public class ExportGridFailures {

    private String source;

    private String password;

    private List<FailureInformationDto> failures;
}
