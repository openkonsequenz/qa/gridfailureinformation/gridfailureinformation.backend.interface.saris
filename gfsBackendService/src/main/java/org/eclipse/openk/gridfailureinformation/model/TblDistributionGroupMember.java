/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import lombok.Data;

import java.util.UUID;

@Data
@Entity
public class TblDistributionGroupMember {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "TBL_DISTR_GROUP_MEMB_ID_SEQ")
    @SequenceGenerator(name = "TBL_DISTR_GROUP_MEMB_ID_SEQ", sequenceName = "TBL_DISTR_GROUP_MEMB_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;
    private UUID uuid;

    @ManyToOne
    @JoinColumn( name = "fk_tbl_distribution_group")
    private TblDistributionGroup tblDistributionGroup;

    private UUID contactId;
    private String postcodes;
}
