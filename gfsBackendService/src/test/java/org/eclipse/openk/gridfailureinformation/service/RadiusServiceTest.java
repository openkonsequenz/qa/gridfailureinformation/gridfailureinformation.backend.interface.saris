/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefRadius;
import org.eclipse.openk.gridfailureinformation.repository.RadiusRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.RadiusDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class RadiusServiceTest {
    @Autowired
    private RadiusService radiusService;

    @Autowired
    private RadiusRepository radiusRepository;

    @Test
    void shouldGetRadiiProperly() {
        List<RefRadius> mockRefRadiusList = MockDataHelper.mockRefRadiusList();
        when(radiusRepository.findAll()).thenReturn(mockRefRadiusList);
        List<RadiusDto> listRefRadius = radiusService.getRadii();

        assertEquals(listRefRadius.size(), mockRefRadiusList.size());
        assertEquals(2, listRefRadius.size());
        assertEquals(listRefRadius.get(1).getUuid(), mockRefRadiusList.get(1).getUuid());
    }


    @Test
    void shouldFindASingleRadiusByValue() {
        RefRadius mockRadius = MockDataHelper.mockRefRadius();
        when(radiusRepository.findByRadius(any(Long.class))).thenReturn(Optional.of(mockRadius));
        RadiusDto radiusDto = radiusService.findByRadius(new Random().nextInt());

        assertEquals( mockRadius.getUuid(), radiusDto.getUuid());
    }
}
