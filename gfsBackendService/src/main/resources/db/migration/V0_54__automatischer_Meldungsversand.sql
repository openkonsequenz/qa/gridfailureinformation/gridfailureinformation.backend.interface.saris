﻿-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------
ALTER TABLE public.TBL_DISTRIBUTION_GROUP RENAME DISTRIBUTION_TEXT TO DISTRIBUTION_TEXT_PUBLISH;
ALTER TABLE public.TBL_DISTRIBUTION_GROUP RENAME EMAIL_SUBJECT TO EMAIL_SUBJECT_PUBLISH;

ALTER TABLE public.TBL_DISTRIBUTION_GROUP ADD COLUMN EMAIL_SUBJECT_COMPLETE varchar(255);
ALTER TABLE public.TBL_DISTRIBUTION_GROUP ADD COLUMN DISTRIBUTION_TEXT_COMPLETE varchar(2048);

ALTER TABLE public.TBL_DISTRIBUTION_GROUP ADD COLUMN EMAIL_SUBJECT_UPDATE varchar(255);
ALTER TABLE public.TBL_DISTRIBUTION_GROUP ADD COLUMN DISTRIBUTION_TEXT_UPDATE varchar(2048);