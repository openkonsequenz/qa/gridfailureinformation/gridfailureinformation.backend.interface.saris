/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.api;


import org.eclipse.openk.gridfailureinformation.api.dto.CommunicationDto;
import org.eclipse.openk.gridfailureinformation.api.dto.VwDetailedContact;
import org.eclipse.openk.gridfailureinformation.api.impl.CustomPageImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.UUID;

@FeignClient(name = "${services.contacts.name}", url = "${services.contacts.url}")
public interface ContactApi {
    @GetMapping("/contacts/{contactUuid}")
    VwDetailedContact findContact(
            @PathVariable( "contactUuid") UUID contactUuid,
            @RequestHeader("Authorization") String token);

    @GetMapping("/contacts/{contactUuid}/communications")
    List<CommunicationDto> getContactCommunications(@PathVariable UUID contactUuid,
                                                           @RequestHeader("Authorization") String token);

    @GetMapping("/contacts")
    CustomPageImpl<VwDetailedContact> findContacts(
            @RequestParam(required = false) String searchText,
            @RequestParam(required = false) String moduleName,
            Pageable pageable,
            @RequestHeader("Authorization") String token
    );
}
