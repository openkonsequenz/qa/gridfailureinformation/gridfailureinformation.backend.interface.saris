/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class DistributionGroupDto {

    @JsonProperty("id")
    private UUID uuid;
    private String name;

    //Long (Mail)
    private String emailSubjectPublish;
    private String distributionTextPublish;

    private String emailSubjectUpdate;
    private String distributionTextUpdate;

    private String emailSubjectComplete;
    private String distributionTextComplete;

    //Short (SMS)
    private String emailSubjectPublishShort;
    private String distributionTextPublishShort;

    private String emailSubjectUpdateShort;
    private String distributionTextUpdateShort;

    private String emailSubjectCompleteShort;
    private String distributionTextCompleteShort;

    private List<DistributionGroupAllowedDto> allowedGroups = new ArrayList<>();
}


