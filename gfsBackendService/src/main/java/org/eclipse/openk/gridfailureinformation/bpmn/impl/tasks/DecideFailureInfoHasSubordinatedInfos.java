/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessSubject;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;

import java.util.List;

@Log4j2
public class DecideFailureInfoHasSubordinatedInfos extends DecisionTask<GfiProcessSubject> {

    public DecideFailureInfoHasSubordinatedInfos() {
        super("Decision: Ist die Störungsinformation aktualisiert?");
    }

    @Override
    public OutputPort decide(GfiProcessSubject subject) throws ProcessException {
        List<FailureInformationDto> children = subject.getProcessHelper().getSubordinatedChildren(
                subject.getFailureInformationDto() );

        String loggerOutput1 = "Decide: ";

        if (!children.isEmpty()) {
            log.debug(loggerOutput1 + getDescription() + "\" -> Firing YES");
            return OutputPort.YES;
        }
        else  {
            log.debug(loggerOutput1 + getDescription() + "\" -> Firing NO");
            return OutputPort.NO;
        }
    }

}
