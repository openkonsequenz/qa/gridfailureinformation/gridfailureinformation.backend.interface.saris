﻿-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------
-- CREATE ROLE GFI_SERVICE LOGIN
-- NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
-- ALTER ROLE GFI_SERVICE with password 'gfi_service';


-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- Tables ADDRESSIMPORT
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

DROP TABLE IF EXISTS public.TBL_ADDRESS CASCADE;
DROP SEQUENCE IF EXISTS public.TBL_ADDRESS_ID_SEQ;

-- ---------------------------------------------
-- TABLE TBL_ADDRESS
-- ---------------------------------------------
CREATE SEQUENCE public.TBL_ADDRESS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.TBL_ADDRESS_ID_SEQ
  OWNER TO GFI_SERVICE;

CREATE TABLE public.TBL_ADDRESS
(
  ID integer NOT NULL DEFAULT nextval('TBL_ADDRESS_ID_SEQ'::regclass),
  UUID uuid NOT NULL,
  SDOX1 numeric(9,2) NOT NULL,
  SDOY1 numeric(10,2) NOT NULL,
  G3EFID numeric,
  POSTCODE character varying(30),
  COMMUNITY character varying(255),
  DISTRICT character varying(255),
  STREET character varying(255),
  HOUSENUMBER character varying(30),
  WATER_CONNECTION boolean,
  WATER_GROUP character varying(255),
  GAS_CONNECTION boolean,
  GAS_GROUP character varying(255),
  POWER_CONNECTION boolean,
  DISTRICTHEATING_CONNECTION boolean,
  TELECOMM_CONNECTION boolean,
  STATION_ID character varying(30),
  LONGITUDE numeric(9,6),
  LATITUDE numeric(9,6),
  CONSTRAINT TBL_ADDRESS_PKEY PRIMARY KEY (id)
);

ALTER TABLE public.TBL_ADDRESS
  OWNER TO GFI_SERVICE;
GRANT ALL ON TABLE public.TBL_ADDRESS TO GFI_SERVICE;

CREATE INDEX idx_tbl_address_g3efid ON public.TBL_ADDRESS ( G3EFID );
CREATE INDEX idx_tbl_address_postcode ON public.TBL_ADDRESS ( POSTCODE );
CREATE INDEX idx_tbl_address_community ON public.TBL_ADDRESS ( COMMUNITY );
CREATE INDEX idx_tbl_address_district ON public.TBL_ADDRESS ( DISTRICT );
CREATE INDEX idx_tbl_address_street ON public.TBL_ADDRESS ( STREET );
CREATE INDEX idx_tbl_address_station_id ON public.TBL_ADDRESS ( STATION_ID );


-- ---------------------------------------------
-- TABLE TBL_STATION
-- ---------------------------------------------

DROP TABLE IF EXISTS public.TBL_STATION CASCADE;
DROP SEQUENCE IF EXISTS public.TBL_STATION_ID_SEQ;

CREATE SEQUENCE public.TBL_STATION_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.TBL_STATION_ID_SEQ
  OWNER TO GFI_SERVICE;

CREATE TABLE public.TBL_STATION
(
  ID integer NOT NULL DEFAULT nextval('TBL_STATION_ID_SEQ'::regclass),
  UUID uuid NOT NULL,
  SDOX1 numeric(9,2) NOT NULL,
  SDOY1 numeric(10,2) NOT NULL,
  G3EFID numeric,
  STATION_ID character varying(30),
  STATION_NAME character varying(255),
  LONGITUDE numeric(9,6),
  LATITUDE numeric(9,6),
  CONSTRAINT TBL_STATION_PKEY PRIMARY KEY (id)
);

ALTER TABLE public.TBL_STATION
  OWNER TO GFI_SERVICE;
GRANT ALL ON TABLE public.TBL_STATION TO GFI_SERVICE;

CREATE INDEX idx_tbl_station_g3efid ON public.TBL_STATION ( G3EFID );
CREATE INDEX idx_tbl_station_station_id ON public.TBL_STATION ( STATION_ID );
CREATE INDEX idx_tbl_station_uuid ON public.TBL_STATION ( UUID );


