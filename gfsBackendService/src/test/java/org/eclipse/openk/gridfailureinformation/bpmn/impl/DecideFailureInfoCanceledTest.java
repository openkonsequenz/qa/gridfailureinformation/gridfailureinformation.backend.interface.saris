/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class DecideFailureInfoCanceledTest {
    @Autowired
    @SpyBean
    private ProcessHelper processHelper;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private FailureInformationRepository failureInformationRepository;

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Test
    public void shouldCall_DecideFailureInfoCanceled_Result_Canceled() {
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();

        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        fiDto.setStatusInternId(UUID.randomUUID());

        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        fiTbl.setId(777L);
        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();

        doReturn(GfiProcessState.CANCELED).when(processHelper).getProcessStateFromStatusUuid(any());
        when(statusRepository.findByUuid(any( UUID.class ))).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);
        doAnswer((Answer<FailureInformationDto>) invocation -> {
            Object[] args = invocation.getArguments();
            return (FailureInformationDto) args[0];
        }).when(processHelper).storeFailureFromViewModel(any(FailureInformationDto.class));
        FailureInformationDto savedDto = processHelper.updateFailureInfo(fiDto);
        //TODO verify CANCELED
        assertEquals(fiDto.getUuid(), savedDto.getUuid());
    }

    @Test
    public void shouldCall_DecideFailureInfoCanceled_Result_Qualified() {
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        fiDto.setStatusInternId(UUID.randomUUID());
        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        fiTbl.setId(777L);
        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();

        when(statusRepository.findByUuid(any())).thenReturn(Optional.of(refStatus));
        doReturn(GfiProcessState.QUALIFIED).when(processHelper).getProcessStateFromStatusUuid(any());
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);
        doAnswer((Answer<FailureInformationDto>) invocation -> {
            Object[] args = invocation.getArguments();
            return (FailureInformationDto) args[0];
        }).when(processHelper).storeFailureFromViewModel(any(FailureInformationDto.class));
        FailureInformationDto savedDto = processHelper.updateFailureInfo(fiDto);
        //TODO verify Qualified
        assertEquals(fiDto.getUuid(), savedDto.getUuid());
    }
}
