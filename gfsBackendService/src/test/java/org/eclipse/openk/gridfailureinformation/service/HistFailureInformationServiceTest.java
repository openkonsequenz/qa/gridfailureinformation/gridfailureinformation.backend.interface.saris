/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class HistFailureInformationServiceTest {
    @Autowired
    private HistFailureInformationService histFailureInformationService;

    @Autowired
    private HistFailureInformationRepository histFailureInformationRepository;

    @Autowired
    @SpyBean
    private HistFailureInformationStationService histFailureInformationStationService;

    @Test
    void shouldFindHistFailureInformationsByUuid() {
        List<HtblFailureInformation> mockHistFailureList = MockDataHelper.mockHistTblFailureInformationList();
        List<StationDto> stationDtoList = MockDataHelper.mockStationDtoList();

        when(histFailureInformationRepository.findByUuid(any())).thenReturn(mockHistFailureList);
        when(histFailureInformationRepository.findByUuidAndVersionNumber(any(), anyLong())).thenReturn(mockHistFailureList);
        when(histFailureInformationStationService.findHistStationsByFailureInfoAndVersionNumber(any(), anyLong())).thenReturn(stationDtoList);
        List<FailureInformationDto> retList = histFailureInformationService.getFailureInformationVersionsByUuid(UUID.randomUUID());

        assertEquals(mockHistFailureList.size(), retList.size());
        assertEquals(2, retList.size());
        assertEquals(retList.get(1).getUuid(), mockHistFailureList.get(1).getUuid());
        assertEquals(retList.get(0).getResponsibility(), mockHistFailureList.get(0).getResponsibility());
        assertEquals(retList.get(0).getStationIds().size(), stationDtoList.size());
        assertEquals(retList.get(0).getStationIds().get(0), stationDtoList.get(0).getUuid());
    }


    @Test
    void shouldFindASingleHistFailureInformationVersion() {
        HtblFailureInformation mockHistFailure = MockDataHelper.mockHistTblFailureInformation();
        List<HtblFailureInformation> mockHistFailureList = new ArrayList<>();
        mockHistFailureList.add(mockHistFailure);
        List<StationDto> stationDtoList = MockDataHelper.mockStationDtoList();

        when(histFailureInformationRepository.findByUuid(any())).thenReturn(mockHistFailureList);
        when(histFailureInformationRepository.findByUuidAndVersionNumber(any(), anyLong())).thenReturn(mockHistFailureList);
        when(histFailureInformationStationService.findHistStationsByFailureInfoAndVersionNumber(any(), anyLong())).thenReturn(stationDtoList);

        FailureInformationDto retDto = histFailureInformationService.getFailureInformationVersion(UUID.randomUUID(), new Random().nextLong());

        assertEquals(mockHistFailure.getResponsibility(), retDto.getResponsibility());
        assertEquals(mockHistFailure.getUuid(), retDto.getUuid());
        assertEquals(stationDtoList.get(0).getUuid(), retDto.getStationIds().get(0));
    }
}
