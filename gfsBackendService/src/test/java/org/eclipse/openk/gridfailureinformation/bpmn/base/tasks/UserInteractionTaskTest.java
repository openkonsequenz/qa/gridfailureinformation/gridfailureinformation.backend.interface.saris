/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base.tasks;

import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.TestProcessSubject;
import org.eclipse.openk.gridfailureinformation.bpmn.base.UserInteractionTaskImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserInteractionTaskTest {
    @Test
    public void testUserinteractionTask() throws ProcessException {
        UserInteractionTaskImpl task=new UserInteractionTaskImpl("UI Task");
        task.stayInTask = false;
        TestProcessSubject subject = new TestProcessSubject();
        task.enterStep( subject );
        assertTrue(task.enterStepCalled);
        assertFalse(task.leaveStepCalled);

        task.leaveStep( subject );
        assertTrue(task.leaveStepCalled);

    }

    @Test
    public void testUserinteractionTaskStayInTask() throws ProcessException {
        UserInteractionTaskImpl task=new UserInteractionTaskImpl("UI Task");
        task.stayInTask = true;
        TestProcessSubject subject = new TestProcessSubject();

        task.enterStep( subject );
        assertTrue(task.enterStepCalled);
        assertFalse(task.leaveStepCalled);

        task.leaveStep( subject );
        assertFalse(task.leaveStepCalled);

    }
}
