/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import org.eclipse.openk.gridfailureinformation.samointerface.SarisInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.samointerface.config.rabbitMq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.samointerface.mapper.SAMOMapper;
import org.eclipse.openk.gridfailureinformation.samointerface.mapper.StoerungsauskunftMapper;
import org.eclipse.openk.gridfailureinformation.samointerface.service.ImportService;
import org.mapstruct.factory.Mappers;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.test.TestRabbitTemplate;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ContextConfiguration;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

@EntityScan(basePackageClasses = SarisInterfaceApplication.class)
@ContextConfiguration( initializers = {ConfigDataApplicationContextInitializer.class})
@EnableRabbit
public class TestConfiguration {
    @Bean
    public ObjectMapper objectMapper() { return new ObjectMapper(); }

    @Bean
    public MessageChannel messageChannel() {return mock(MessageChannel.class);}

    @Bean
    public RabbitMqConfig rabbitMqConfig() {
        RabbitMqConfig rabbitMqConfigMock = mock(RabbitMqConfig.class);
        doNothing().when(rabbitMqConfigMock).buildAllQueues();
        return rabbitMqConfigMock;
    }

    @Bean
    public TestRabbitTemplate testRabbitTemplate() {
        return new TestRabbitTemplate(connectionFactory());
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        ConnectionFactory factory = mock(ConnectionFactory.class);
        Connection connection = mock(Connection.class);
        Channel channel = mock(Channel.class);
        willReturn(connection).given(factory).createConnection();
        willReturn(channel).given(connection).createChannel(anyBoolean());
        given(channel.isOpen()).willReturn(true);
        return factory;
    }

    @Bean
    public ImportService importService() {
        return new ImportService(
                messageChannel(),
                objectMapper(),
                Mappers.getMapper(SAMOMapper.class)
        );
    }

    @Bean
    public StoerungsauskunftMapper stoerungsauskunftMapper() {
        return Mappers.getMapper(StoerungsauskunftMapper.class);
    }
}
