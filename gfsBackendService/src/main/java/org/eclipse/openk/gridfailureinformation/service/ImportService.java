/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.util.ImportDataValidator;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.ImportDataDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.RadiusDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@Validated
@Service
public class ImportService {
    private final RabbitMqConfig rabbitMqConfig;

    private final FailureInformationService failureInformationService;

    private final BranchService branchService;

    private final RadiusService radiusService;

    private final StatusService statusService;

    private final StationService stationService;

    private final AddressRepository addressRepository;

    private final FailureInformationMapper failureInformationMapper;

    private final ImportDataValidator importDataValidator;

    private final ProcessHelper processHelper;

    private final ObjectMapper objectMapper;

    public ImportService(RabbitMqConfig rabbitMqConfig, FailureInformationService failureInformationService, BranchService branchService, RadiusService radiusService, StatusService statusService, StationService stationService, AddressRepository addressRepository, FailureInformationMapper failureInformationMapper, ImportDataValidator importDataValidator, ProcessHelper processHelper, ObjectMapper objectMapper) {
        this.rabbitMqConfig = rabbitMqConfig;
        this.failureInformationService = failureInformationService;
        this.branchService = branchService;
        this.radiusService = radiusService;
        this.statusService = statusService;
        this.stationService = stationService;
        this.addressRepository = addressRepository;
        this.failureInformationMapper = failureInformationMapper;
        this.importDataValidator = importDataValidator;
        this.processHelper = processHelper;
        this.objectMapper = objectMapper;
    }

    @Transactional
    public boolean validateAndImport(ImportDataDto importDataDto) {
        ForeignFailureDataDto foreignFailureDataDto = importDataValidator.readSafeForeignFailureInfo(importDataDto);

        if (foreignFailureDataDto != null) {
            doImport(importDataDto, foreignFailureDataDto);
            return true;
        }
        else {
            return false;
        }
    }


    @Transactional
    public FailureInformationDto doImport(ImportDataDto importDataDto, ForeignFailureDataDto foreignFailureDataDto) {
        FailureInformationDto existingDto = failureInformationService.findByObjectReferenceExternalSystem(importDataDto.getAssembledRefId());

        if (existingDto == null) {
            importNewFailureInfo(importDataDto, foreignFailureDataDto);
        } else {
            importExistingDto(importDataDto, foreignFailureDataDto, existingDto);
        }
        return existingDto;
    }

    private void importExistingDto(ImportDataDto importDataDto, ForeignFailureDataDto foreignFailureDataDto, FailureInformationDto existingDto) {
        if (foreignFailureDataDto.isOnceOnlyImport()) {
            log.debug("External failure information [MetaId: " + importDataDto.getMetaId() + "] from "
                    + importDataDto.getSource() + " won't be imported because it has already been imported" +
                    " once and flag isOnceOnlyImport is true.");
            return;
        }

        UUID statusInternIdExistingDto = existingDto.getStatusInternId();

        //When status of already existing failureinformation completed or canceled do not update but log it.
        if (statusInternIdExistingDto.equals(statusService.getStatusFromId(GfiProcessState.COMPLETED.getStatusValue()).getUuid()) ||
                statusInternIdExistingDto.equals(statusService.getStatusFromId(GfiProcessState.CANCELED.getStatusValue()).getUuid())) {
            log.info("External failure information [MetaId: " + importDataDto.getMetaId() + "] from " + importDataDto.getSource() +
                    " tried to update already existing failure information [UUID: "+ existingDto.getUuid() + "]" +
                    " but was ignored since its status is already CANCELED or COMPLETED.");
            return;
        }

        FailureInformationDto existingDtoClone = null;
        try {
            existingDtoClone = objectMapper
                    .readValue(objectMapper.writeValueAsString(existingDto), FailureInformationDto.class);
        } catch (JsonProcessingException e) {
            log.error("Error parsing object (JSON)", e);
        }

        FailureInformationDto updatedDto = setUpdateFromForeignDto(importDataDto, foreignFailureDataDto, existingDto);

        //Is the content of the new failureinformation different compared to the already existing one
        if (foreignFailureDataDto.isExcludeEquals() && updatedDto.equals(existingDtoClone)){
            log.debug("External failure information [MetaId: " + importDataDto.getMetaId() + "] from "
                    + importDataDto.getSource() + " hasn't changed, no update will be executed.");
            return;
        }


        Assert.notNull(existingDtoClone, "existingDtoClone was null");
        //Don't update if the failureinformation was already edited by an user
        if (foreignFailureDataDto.isExcludeAlreadyEdited() && !Objects.equals(updatedDto.getModUser(), existingDtoClone.getModUser())){ //NOSONAR assertion for existingDtoClone exists and logically existingDtoClone must be valid
            log.debug("External failure information [MetaId: " + importDataDto.getMetaId() + "] from "
                    + importDataDto.getSource() + " has already been edited in SIT by an user, no update will be executed.");
            return;
        }

        if(!foreignFailureDataDto.isAutopublish() && statusInternIdExistingDto.equals(
                statusService.getStatusFromId(GfiProcessState.QUALIFIED.getStatusValue()).getUuid()
        )) {
            updatedDto.setStatusInternId(
                    statusService.getStatusFromId(GfiProcessState.UPDATED.getStatusValue()).getUuid()
            );
        } else {
            if (foreignFailureDataDto.isAutopublish()) {
                log.info("Autopublish for external failure information [MetaId: " + importDataDto.getMetaId() + "] from "
                        + importDataDto.getSource() + " is true, current status remains unchanged.");
            }
        }

        processHelper.updateFailureInfo(updatedDto);
        log.info("External failure information [" + importDataDto.getMetaId() + "] from " + importDataDto.getSource() + " imported (updated).");
    }

    private void importNewFailureInfo(ImportDataDto importDataDto, ForeignFailureDataDto foreignFailureDataDto) {
        FailureInformationDto failureInformationDto;
        failureInformationDto = setNewFromForeignDto(importDataDto, foreignFailureDataDto);

        GfiProcessState gfiProcessState;
        if (foreignFailureDataDto.isPlanned()) {
            gfiProcessState = GfiProcessState.PLANNED;
        } else {
            gfiProcessState = GfiProcessState.NEW;
        }

        if (foreignFailureDataDto.isAutopublish()) {
            gfiProcessState = GfiProcessState.QUALIFIED;
            failureInformationDto.setPublicationStatus(Constants.PUB_STATUS_VEROEFFENTLICHT);
        }

        FailureInformationDto retVal = failureInformationService.insertFailureInfo(failureInformationDto, gfiProcessState);
        log.info("External failure information [MetaId: " + importDataDto.getMetaId() + "] from "
                + importDataDto.getSource() + " imported (inserted).");

        if (foreignFailureDataDto.isAutopublish()) {
            String webComponentPublicationChannel = rabbitMqConfig.getChannelTypeToNameMap().get(Constants.CHANNEL_TPYE_PUBLICATION_OWNDMZ);
            failureInformationService.insertPublicationChannelForFailureInfo(retVal.getUuid(), webComponentPublicationChannel, true);
            log.info("External failure information [MetaId: " + importDataDto.getMetaId() + "] from "
                    + importDataDto.getSource() + " is prepared for autopublish to channel: " + webComponentPublicationChannel);
        }
    }

    public FailureInformationDto setNewFromForeignDto(ImportDataDto importDataDto, ForeignFailureDataDto foreignFailureDataDto) {
        FailureInformationDto failureInformationDto = failureInformationMapper.mapForeignFiDtoToGfiDto(foreignFailureDataDto);


        setFromForeignDto(importDataDto, foreignFailureDataDto, failureInformationDto);
        failureInformationDto.setCreateUser(importDataDto.getSource());
        failureInformationDto.setModUser(importDataDto.getSource());
        return failureInformationDto;
    }

    public FailureInformationDto setUpdateFromForeignDto(ImportDataDto importDataDto, ForeignFailureDataDto foreignFailureDataDto, FailureInformationDto failureInformationDto) {
        setFromForeignDto(importDataDto, foreignFailureDataDto, failureInformationDto);
        failureInformationDto.setModUser(importDataDto.getSource());
        return failureInformationDto;
    }


    private void setFromForeignDto(ImportDataDto importDataDto, ForeignFailureDataDto foreignFailureDataDto, FailureInformationDto failureInformationDto) {
        failureInformationDto.setFailureBegin(foreignFailureDataDto.getFailureBegin());
        failureInformationDto.setFailureEndPlanned(foreignFailureDataDto.getFailureEndPlanned());
        failureInformationDto.setFailureEndResupplied(foreignFailureDataDto.getFailureEndResupplied());
        failureInformationDto.setDescription(foreignFailureDataDto.getDescription());
        failureInformationDto.setPressureLevel(foreignFailureDataDto.getPressureLevel());
        failureInformationDto.setStationDescription(foreignFailureDataDto.getStationDescription());
        String foreignStationId = foreignFailureDataDto.getStationId();
        failureInformationDto.setStationId(foreignStationId);

        failureInformationDto.setPostcode(foreignFailureDataDto.getPostcode());
        failureInformationDto.setCity(foreignFailureDataDto.getCity());
        failureInformationDto.setDistrict(foreignFailureDataDto.getDistrict());
        failureInformationDto.setStreet(foreignFailureDataDto.getStreet());
        failureInformationDto.setHousenumber(foreignFailureDataDto.getHousenumber());

        setCoordinates(foreignFailureDataDto, failureInformationDto);
        setLocationType(failureInformationDto);

        failureInformationDto.setStationIds(new LinkedList<>());
        if( foreignStationId != null && !foreignStationId.isEmpty()) {
            failureInformationDto.setFaultLocationArea(Constants.LOCATION_TYPE_STATION);
            importStation(failureInformationDto.getStationIds(), foreignStationId);
        }

        failureInformationDto.setVoltageLevel(foreignFailureDataDto.getVoltageLevel());

        String branchName = foreignFailureDataDto.getBranch();
        branchName = branchName == null || branchName.isEmpty() ? "OS" : branchName;
        failureInformationDto.setBranchId(branchService.findByName(branchName).getUuid());
        failureInformationDto.setBranch(foreignFailureDataDto.getBranch());

        if(foreignFailureDataDto.getRadiusInMeters() != null) {
            importRadius(foreignFailureDataDto.getRadiusInMeters(), failureInformationDto);
        }
        failureInformationDto.setRadius(foreignFailureDataDto.getRadiusInMeters());
        failureInformationDto.setObjectReferenceExternalSystem(importDataDto.getAssembledRefId());
    }

    private void setLocationType(FailureInformationDto failureInformationDto) {
        failureInformationDto.setFaultLocationArea(Constants.LOCATION_TYPE_ADDRESS);
        if (failureInformationDto.getLatitude() != null && failureInformationDto.getLongitude() != null
        && Strings.isBlank(failureInformationDto.getStreet())) {
            failureInformationDto.setFaultLocationArea(Constants.LOCATION_TYPE_MAP);
            failureInformationDto.setFreetextCity(failureInformationDto.getCity());
            failureInformationDto.setFreetextDistrict(failureInformationDto.getDistrict());
            failureInformationDto.setFreetextPostcode(failureInformationDto.getPostcode());
        }
    }

    private void setCoordinates(ForeignFailureDataDto foreignFailureDataDto, FailureInformationDto failureInformationDto) {
        BigDecimal latitude = foreignFailureDataDto.getLatitude();
        BigDecimal longitude = foreignFailureDataDto.getLongitude();

        if (Strings.isNotBlank(failureInformationDto.getHousenumber())) {
            List<TblAddress> uniqueAddressList =
                    addressRepository.findByCompleteAdress(failureInformationDto.getPostcode(),
                            failureInformationDto.getCity(), failureInformationDto.getDistrict(),
                            failureInformationDto.getStreet(), failureInformationDto.getHousenumber());
            if (!uniqueAddressList.isEmpty()) {
                TblAddress uniqueTblAddress = uniqueAddressList.get(0);
                latitude = uniqueTblAddress.getLatitude();
                longitude = uniqueTblAddress.getLongitude();
            }
        }

        failureInformationDto.setLatitude(latitude);
        failureInformationDto.setLongitude(longitude);
    }

    private void importStation(List<UUID> targetList, String stationIdToImport) {
        StationDto selectedStation = stationService.getStationsById( stationIdToImport );
        if( selectedStation != null ) {
            targetList.add(selectedStation.getUuid());
        }
        if( !stationIdToImport.isEmpty() && targetList.isEmpty() ) {
            log.warn("Import message delivered a stationId("+stationIdToImport+") that could not be resolved!");
        }
    }

    private void importRadius(long foreignRadius, FailureInformationDto failureInformationDto) {
        Optional<RadiusDto> bestFitHigh = radiusService.getRadii().stream()
                .filter( r -> r.getRadius() >= foreignRadius )
                .min(Comparator.comparingLong(RadiusDto::getRadius));

        Optional<RadiusDto> bestFitLow = radiusService.getRadii().stream()
                .filter( r -> r.getRadius() < foreignRadius )
                .max(Comparator.comparingLong(RadiusDto::getRadius));

        UUID bestFitUUID;
        if( bestFitHigh.isPresent() && bestFitLow.isPresent()) {
            if (foreignRadius > (bestFitHigh.get().getRadius() - bestFitLow.get().getRadius()) / 2) {
                bestFitUUID = bestFitHigh.get().getUuid();
            } else {
                bestFitUUID = bestFitLow.get().getUuid();
            }
        }
        else if( bestFitHigh.isPresent()) {
            bestFitUUID = bestFitHigh.get().getUuid();
        }
        else if( bestFitLow.isPresent()) {
            bestFitUUID = bestFitLow.get().getUuid();
        }
        else {
            throw new InternalServerErrorException("corrupt radii-configuration");
        }

        failureInformationDto.setRadiusId(bestFitUUID);
    }
}
