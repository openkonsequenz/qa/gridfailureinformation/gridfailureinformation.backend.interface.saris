/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqChannel;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqProperties;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.enums.OperationType;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.exceptions.OperationDeniedException;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationPublicationChannelMapper;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.RefBranch;
import org.eclipse.openk.gridfailureinformation.model.RefExpectedReason;
import org.eclipse.openk.gridfailureinformation.model.RefFailureClassification;
import org.eclipse.openk.gridfailureinformation.model.RefRadius;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationPublicationChannel;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationReminderMailSent;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.repository.BranchRepository;
import org.eclipse.openk.gridfailureinformation.repository.ExpectedReasonRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureClassificationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationDistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationPublicationChannelRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationReminderMailSentRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.RadiusRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.util.ExternalStatusCalculator;
import org.eclipse.openk.gridfailureinformation.util.GrahamScan;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationLastModDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationPublicationChannelDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationPolygonDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.geojson.Feature;
import org.eclipse.openk.gridfailureinformation.viewmodel.geojson.StationGeoJson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.Point;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
public class FailureInformationService {
    @Value("${spring.settings.daysInPastToShowClosedInfos}")
    private int daysInPastToShowClosedInfos;

    @Value("#{T(java.util.Arrays).asList('${polygonBranches:}')}")
    private List<String> polygonBranches;

    @Value("${process.definitions.classification.plannedMeasureDbid}")
    private long plannedMeasureDbId;

    @Getter
    private final FailureInformationRepository failureInformationRepository;

    private final HistFailureInformationRepository histFailureInformationRepository;

    private final FailureInformationMapper failureInformationMapper;

    private final FailureInformationPublicationChannelMapper failureInformationPublicationChannelMapper;

    private final BranchRepository branchRepository;

    private final FailureClassificationRepository failureClassificationRepository;

    private final RadiusRepository radiusRepository;

    private final ExpectedReasonRepository expectedReasonRepository;

    private final StatusRepository statusRepository;

    private final StationRepository stationRepository;

    private final AddressRepository addressRepository;

    private final FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository;

    @Getter
    private final FailureInformationPublicationChannelRepository failureInformationPublicationChannelRepository;

    private final FailureInformationStationRepository failureInformationStationRepository;

    private final FailureInformationReminderMailSentRepository failureInformationReminderMailSentRepository;

    private final HistFailureInformationStationService histFailureInformationStationService;

    private final RabbitMqConfig rabbitMqConfig;

    private final ObjectMapper objectMapper;

    public FailureInformationService(FailureInformationRepository failureInformationRepository, HistFailureInformationRepository histFailureInformationRepository, FailureInformationMapper failureInformationMapper, FailureInformationPublicationChannelMapper failureInformationPublicationChannelMapper, BranchRepository branchRepository, FailureClassificationRepository failureClassificationRepository, RadiusRepository radiusRepository, ExpectedReasonRepository expectedReasonRepository, StatusRepository statusRepository, StationRepository stationRepository, AddressRepository addressRepository, FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository, FailureInformationPublicationChannelRepository failureInformationPublicationChannelRepository, FailureInformationStationRepository failureInformationStationRepository, FailureInformationReminderMailSentRepository failureInformationReminderMailSentRepository, HistFailureInformationStationService histFailureInformationStationService, RabbitMqConfig rabbitMqConfig, ObjectMapper objectMapper) {
        this.failureInformationRepository = failureInformationRepository;
        this.histFailureInformationRepository = histFailureInformationRepository;
        this.failureInformationMapper = failureInformationMapper;
        this.failureInformationPublicationChannelMapper = failureInformationPublicationChannelMapper;
        this.branchRepository = branchRepository;
        this.failureClassificationRepository = failureClassificationRepository;
        this.radiusRepository = radiusRepository;
        this.expectedReasonRepository = expectedReasonRepository;
        this.statusRepository = statusRepository;
        this.stationRepository = stationRepository;
        this.addressRepository = addressRepository;
        this.failureInformationDistributionGroupRepository = failureInformationDistributionGroupRepository;
        this.failureInformationPublicationChannelRepository = failureInformationPublicationChannelRepository;
        this.failureInformationStationRepository = failureInformationStationRepository;
        this.failureInformationReminderMailSentRepository = failureInformationReminderMailSentRepository;
        this.histFailureInformationStationService = histFailureInformationStationService;
        this.rabbitMqConfig = rabbitMqConfig;
        this.objectMapper = objectMapper;
    }

    public void setDefaultChannels(FailureInformationDto failureInformationDto) {
        RabbitMqProperties rabbitMqProperties = rabbitMqConfig.getRabbitMqProperties();
        List<RabbitMqChannel> rabbitMqChannelList = rabbitMqProperties.getChannels();
        for (RabbitMqChannel rabbitMqChannel : rabbitMqChannelList) {
            if (rabbitMqChannel.isInitialDefault()) {
                insertPublicationChannelForFailureInfo(failureInformationDto.getUuid(), rabbitMqChannel.getName(), false);
            }
        }
    }

    public FailureInformationDto findFailureInformation(UUID uuid) {
        TblFailureInformation existingTblFailureInfo = failureInformationRepository.findByUuid(uuid)
                .orElseThrow(NotFoundException::new);

        return enrichFailureInfo(failureInformationMapper.toFailureInformationDto(existingTblFailureInfo));
    }

    public Page<FailureInformationDto> findFailureInformations(Pageable pageable) {
        Page<FailureInformationDto> retPage = findFailureInformationsForDisplay(pageable)
                .map(failureInformationMapper::toFailureInformationDto);

        retPage.getContent().forEach(this::enrichFailureInfo);
        return retPage;
    }

    public Page<TblFailureInformation> findFailureInformationsForDisplay(Pageable pageable) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime ldt = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 0, 0, 0);
        ldt = ldt.minusDays(daysInPastToShowClosedInfos);
        Date nowMinusFourWeeks = Date.from(ldt.atZone(ZoneId.of("UTC")).toInstant());
        return failureInformationRepository.findByTblFailureInformationForDisplay(
                GfiProcessState.COMPLETED.getStatusValue(),
                GfiProcessState.CANCELED.getStatusValue(),
                nowMinusFourWeeks, pageable);
    }

    public List<FailureInformationDto> findFailureInformationsByCondensedUuid(UUID uuid) {

        List<FailureInformationDto> listFailureInformationDtos = new LinkedList<>();
        List<TblFailureInformation> listFailureInformations =  failureInformationRepository.findByFailureInformationCondensedUuid(uuid);

        for(TblFailureInformation tblInfo: listFailureInformations){
            FailureInformationDto infoDto = failureInformationMapper.toFailureInformationDto(tblInfo);
            enrichFailureInfo(infoDto);
            listFailureInformationDtos.add(infoDto);
        }

        return listFailureInformationDtos;
    }

    public FailureInformationDto findByObjectReferenceExternalSystem( String extRef ) {
        Optional<TblFailureInformation> tblFailureInformation
                =  failureInformationRepository.findByObjectReferenceExternalSystem( extRef );
        return tblFailureInformation.map(failureInformationMapper::toFailureInformationDto).orElse(null);
    }

    @Transactional
    public FailureInformationDto insertFailureInfo(FailureInformationDto failureInfoDto,  GfiProcessState initialState) {
        failureInfoDto.setUuid(null); // force null here

        return storeFailureInfo(failureInfoDto, initialState);
    }

    public FailureInformationDto storeFailureInfo(FailureInformationDto dto, GfiProcessState initialState) {
        TblFailureInformation tblFailureInformationToSave = failureInformationMapper.toTblFailureInformation(dto);
        if (dto.getUuid() == null) {
            tblFailureInformationToSave.setUuid(UUID.randomUUID());
            tblFailureInformationToSave.setVersionNumber(1L);
            setFromGridFailureInformationDto(tblFailureInformationToSave, dto);

            RefStatus refStatus = statusRepository.findById(initialState.getStatusValue())
                    .orElseThrow(() -> new NotFoundException("ref.status.not.found"));

            tblFailureInformationToSave.setRefStatusIntern(refStatus);
            tblFailureInformationToSave.setCondensedCount(0);
            tblFailureInformationToSave.setCondensed(false);
        } else {
            TblFailureInformation tblFailureInformation = failureInformationRepository.findByUuid(dto.getUuid())
                    .orElseThrow(() -> new NotFoundException(Constants.FAILURE_INFO_UUID_NOT_EXISTING));

            tblFailureInformationToSave = failureInformationMapper.toTblFailureInformation(dto);
            tblFailureInformationToSave.setId(tblFailureInformation.getId());

            setFromGridFailureInformationDto(tblFailureInformationToSave, dto);
            setVersionNumber(tblFailureInformationToSave);
        }

        TblFailureInformation tblFailureInformationSaved = failureInformationRepository.save(tblFailureInformationToSave);
        FailureInformationDto failureInformationDto = failureInformationMapper.toFailureInformationDto(tblFailureInformationSaved);

        storeStations(tblFailureInformationSaved, dto.getStationIds());
        FailureInformationDto enrichedFailureInformationDto = enrichFailureInfo(failureInformationDto);

        // insert stations in history table
        histFailureInformationStationService.insertHistFailureInfoStationsForGfi(tblFailureInformationSaved);

        return enrichedFailureInformationDto;
    }

    private void storeStations(TblFailureInformation tblToStore, List<UUID> stationIds) {
        List<TblStation> stationList = resolveStationIds(stationIds);
        Set<TblStation> stationSet = new HashSet<>(stationList);
        if(stationList.size() > stationSet.size()){
            throw new OperationDeniedException(OperationType.INSERT, "double.assignment.of.stations");
        }

        if (tblToStore != null) {
            List<TblFailinfoStation> tblFailureStations
                    = failureInformationStationRepository.findByFailureInformationId(tblToStore.getId());
            failureInformationStationRepository.deleteAll(tblFailureStations);
        }

        stationList.forEach(x-> {
            TblFailinfoStation newRecord = new TblFailinfoStation();
            newRecord.setStationStationId(x.getStationId());
            newRecord.setFailureInformation(tblToStore);
            failureInformationStationRepository.save(newRecord);
        });
    }

    public RefStatus getRefStatus(FailureInformationDto failureInfoDto) {
        FailureInformationDto failureInfoDtoDB = findFailureInformation(failureInfoDto.getUuid());
        return statusRepository.findByUuid(failureInfoDtoDB.getStatusInternId())
                .orElseThrow(() -> new InternalServerErrorException("status.not.found.or.null"));
    }

    private void setFromGridFailureInformationDto(TblFailureInformation destTblFailureInformation, FailureInformationDto sourceDto) {
        resolveBranch(destTblFailureInformation, sourceDto);
        resolveFailureClassification(destTblFailureInformation, sourceDto);
        resolveStatii(destTblFailureInformation, sourceDto);
        resolveRadius(destTblFailureInformation, sourceDto);
        resolveExpectedReason(destTblFailureInformation, sourceDto);
        resolveCondensed(destTblFailureInformation, sourceDto);

        destTblFailureInformation.setStations(resolveStationIds(sourceDto.getStationIds()));
    }


    private List<TblStation> resolveStationIds(List<UUID> stationUuids) {
        if( stationUuids != null ) {
            List<TblStation> stationList = new ArrayList<>();

            for (UUID stationUuid: stationUuids) {
                stationList.add(stationRepository.findByUuid(stationUuid).orElseThrow(() -> new NotFoundException("station.uuid.not.existing")));
            }
            return stationList;

        }
        return new LinkedList<>();
    }

    private void resolveCondensed(TblFailureInformation destTblFailureInformation, FailureInformationDto sourceDto) {
        if( sourceDto.getFailureInformationCondensedId() != null ) {
            destTblFailureInformation.setTblFailureInformationCondensed( failureInformationRepository
                    .findByUuid(sourceDto.getFailureInformationCondensedId())
                    .orElseThrow(() -> new NotFoundException(Constants.FAILURE_INFO_UUID_NOT_EXISTING)));
        }
        else {
            destTblFailureInformation.setTblFailureInformationCondensed(null);
        }
    }

    private void resolveExpectedReason(TblFailureInformation destTblFailureInformation, FailureInformationDto sourceDto) {
        if( sourceDto.getExpectedReasonId() != null ) {
            destTblFailureInformation.setRefExpectedReason( expectedReasonRepository
                    .findByUuid(sourceDto.getExpectedReasonId())
                    .orElseThrow(() -> new NotFoundException("expected.reason.uuid.not.existing")));
        }
        else {
            destTblFailureInformation.setRefExpectedReason(null);
        }
    }

    private void resolveRadius(TblFailureInformation destTblFailureInformation, FailureInformationDto sourceDto) {
        if( sourceDto.getRadiusId() != null ) {
            destTblFailureInformation.setRefRadius(radiusRepository
                    .findByUuid(sourceDto.getRadiusId())
                    .orElseThrow(() -> new NotFoundException("radius.uuid.not.existing")));
        }
        else {
            destTblFailureInformation.setRefRadius(null);
        }
    }

    private void resolveStatii(TblFailureInformation destTblFailureInformation, FailureInformationDto sourceDto) {
        if( sourceDto.getStatusInternId() != null ) {
            destTblFailureInformation.setRefStatusIntern( statusRepository
                    .findByUuid(sourceDto.getStatusInternId())
                    .orElseThrow(() -> new NotFoundException("status.uuid.not.existing")));
        }
        else {
            destTblFailureInformation.setRefStatusIntern(null);
        }
    }

    private void resolveFailureClassification(TblFailureInformation destTblFailureInformation, FailureInformationDto sourceDto) {
        if(sourceDto.getFailureClassificationId() != null ) {
            destTblFailureInformation.setRefFailureClassification(failureClassificationRepository
                    .findByUuid(sourceDto.getFailureClassificationId())
                    .orElseThrow(() -> new NotFoundException("failure.classification.uuid.not.existing")));
        }
        else {
            destTblFailureInformation.setRefFailureClassification(null);
        }
    }

    private void resolveBranch(TblFailureInformation destTblFailureInformation, FailureInformationDto sourceDto) {
        if (sourceDto.getBranchId() != null) {
            destTblFailureInformation.setRefBranch(branchRepository
                    .findByUuid(sourceDto.getBranchId())
                    .orElseThrow(() -> new NotFoundException("branch.uuid.not.existing")));
        }
        else {
            destTblFailureInformation.setRefBranch(null);
        }
    }

    private void setVersionNumber(TblFailureInformation tblFailureInformation){
        List<HtblFailureInformation> hfailureList = histFailureInformationRepository.findByUuid(tblFailureInformation.getUuid());
        Long lastVersion = hfailureList.stream().map(HtblFailureInformation::getVersionNumber).max(Comparator.naturalOrder()).orElse(0L) ;
        tblFailureInformation.setVersionNumber(lastVersion+1);
    }


    public FailureInformationDto enrichFailureInfo(FailureInformationDto failureInformationDto) {
        // wenn Mittelspannung oder bei definierten Branches dann füge Adress-Polygonpunkte für Kartenanzeige hinzu
        if (failureInformationDto.getBranch() != null && polygonBranches.contains((failureInformationDto.getBranch()).toUpperCase())
                || (failureInformationDto.getVoltageLevel() != null && failureInformationDto.getVoltageLevel().equals("MS"))) {
            addPolygonAddressPoints(failureInformationDto);
        }

        ExternalStatusCalculator.addExternalStatus(statusRepository, failureInformationDto);

        enrichStations(failureInformationDto);

        return failureInformationDto;
    }


    @Transactional
    public FailureInformationDto updateSubordinatedFailureInfos(UUID condensedUuid, List<UUID> subordinatedUuidList){
        return condenseFailureInfos(subordinatedUuidList, Optional.of(condensedUuid));
    }

    private void storeSubordinated(TblFailureInformation child, TblFailureInformation parent) {
        child.setCondensed(false);
        child.setTblFailureInformationCondensed(parent);
        setVersionNumber(child);
        failureInformationRepository.save(child);
    }

    protected boolean containsAlienSubordinatedFailureInfos(Optional<UUID> condensedUuid, List<UUID> subordinatedUuidList){
        List<TblFailureInformation> futureSubordinateFailureInfoList = failureInformationRepository
                .findByUuidIn(subordinatedUuidList);

        List<TblFailureInformation> filteredList =  futureSubordinateFailureInfoList.stream()
                .filter(x -> x.getTblFailureInformationCondensed() != null)
                .collect(Collectors.toList());

        if(condensedUuid.isPresent()) {
            filteredList = filteredList.stream()
                    .filter(x -> !x.getTblFailureInformationCondensed().getUuid().equals(condensedUuid.get()) )
                    .toList();
        }
        return !filteredList.isEmpty();
    }

    private void enrichStations( FailureInformationDto dto) {
        List<UUID> stationList = failureInformationStationRepository.findUuidByFkTblFailureInformation(dto.getUuid());
        dto.setStationIds(stationList);
    }

    @Transactional
    public FailureInformationDto condenseFailureInfos(List<UUID> listUuids, Optional<UUID> condensedUuid){
        //  pruefen, ob in der uebergebenen Liste unterzuordnender FailureInfos Teile einer anderen Verdichtung sind
        if (containsAlienSubordinatedFailureInfos(condensedUuid, listUuids)) {
            throw new BadRequestException("failure.info.already.subordinated");
        }

        if(listUuids.isEmpty()){
            throw new BadRequestException("empty.array.for.subordinated.failure.infos");
        }

        List<TblFailureInformation> listNewSubordinatedFailureInfos= failureInformationRepository.findByUuidIn(listUuids);
        List<TblFailureInformation> oldListOfSubordinatedFailureInfos = (condensedUuid.isPresent() ?
                failureInformationRepository.findByFailureInformationCondensedUuid(condensedUuid.get()) : new ArrayList<>());

        if(!checkSameBranch(listNewSubordinatedFailureInfos)) {
            throw new OperationDeniedException(OperationType.CONDENSE, "failure.infos.have.different.branches");
        }

        TblFailureInformation condensedFailureInformation = getOrCreateCondensedFailureInfo(condensedUuid);

        condensedFailureInformation.setRefBranch(listNewSubordinatedFailureInfos.get(0).getRefBranch());

        setCondensedFieldsFromMany(condensedFailureInformation, listNewSubordinatedFailureInfos);
        condensedFailureInformation.setCondensed(true);
        condensedFailureInformation.setCondensedCount(listNewSubordinatedFailureInfos.size());
        TblFailureInformation condensedFailureInformationSaved = failureInformationRepository.save(condensedFailureInformation);

        storeCondensedStations(listNewSubordinatedFailureInfos, condensedFailureInformationSaved);

        //kennzeichne die neu hingefügt FailureInfos und speichere
        listNewSubordinatedFailureInfos.stream()
                .filter( x -> listUuids.contains(x.getUuid()))
                .forEach( x -> storeSubordinated(x, condensedFailureInformationSaved));

        // die alten nicht mehr zugeordneten FailureInfos freigeben
        oldListOfSubordinatedFailureInfos.stream()
                .filter( x -> !listUuids.contains(x.getUuid()))
                .forEach( x -> storeSubordinated(x, null));

        return enrichFailureInfo(failureInformationMapper.toFailureInformationDto(condensedFailureInformationSaved));
    }

    private void setCondensedFieldsFromMany(TblFailureInformation condensedFailureInformation, List<TblFailureInformation> listSubordinatedFailureInfos) {
        setVoltageLevel(listSubordinatedFailureInfos, condensedFailureInformation);
        setEarliestStartdate(listSubordinatedFailureInfos, condensedFailureInformation);
        setLatestEnddate(listSubordinatedFailureInfos, condensedFailureInformation);
        setExpectedReason(listSubordinatedFailureInfos, condensedFailureInformation);
        setAddress(listSubordinatedFailureInfos, condensedFailureInformation);
        setRadius(listSubordinatedFailureInfos, condensedFailureInformation);

        RefStatus refStatusNew = statusRepository
                .findById(GfiProcessState.NEW.getStatusValue())
                .orElseThrow(() -> new NotFoundException("status.not.existing"));

        condensedFailureInformation.setRefStatusIntern(refStatusNew);
    }

    private TblFailureInformation getOrCreateCondensedFailureInfo(Optional<UUID> condensedUuid) {
        TblFailureInformation condensedFailureInformation;
        if(condensedUuid.isPresent()){
            // update einer bestehenden Verdichtung
            condensedFailureInformation = failureInformationRepository
                    .findByUuid(condensedUuid.get())
                    .orElseThrow(() -> new NotFoundException(Constants.FAILURE_INFO_UUID_NOT_EXISTING));
            this.setVersionNumber(condensedFailureInformation);
        }
        else {
            // neue Verdichtung
            condensedFailureInformation = new TblFailureInformation();
            condensedFailureInformation.setUuid(UUID.randomUUID());
            condensedFailureInformation.setVersionNumber(1L);
        }
        return condensedFailureInformation;
    }

    private void storeCondensedStations(List<TblFailureInformation> listFailureInfos, TblFailureInformation condensedFailureInformation) {
        // get unique set of station (strings) from all subordinated failureinfos
        Set<String> stationsSuperSet = new HashSet<>();
        listFailureInfos.forEach( x ->
            stationsSuperSet.addAll(
                failureInformationStationRepository.findByFailureInformationId(x.getId())
                    .stream()
                        .map( TblFailinfoStation::getStationStationId )
                        .toList()
            )
        );
        final List<TblFailinfoStation> byFkTblFailureInformation = failureInformationStationRepository.findByFailureInformationId(condensedFailureInformation.getId());
        Set<String> stationsOfOldExistingCondensedFi =
                byFkTblFailureInformation.stream()
                .map( TblFailinfoStation::getStationStationId )
                .collect(Collectors.toSet());

        // store the set for the newly created failure info
        stationsSuperSet
                .stream()
                .filter( x -> !stationsOfOldExistingCondensedFi.contains(x))
                .forEach( x -> {
                    TblFailinfoStation fis = new TblFailinfoStation();
                    fis.setFailureInformation(condensedFailureInformation);
                    fis.setStationStationId(x);
                    failureInformationStationRepository.save(fis);
        });
        // remove obsolete stations
        byFkTblFailureInformation.stream()
                .filter( x -> !stationsSuperSet.contains(x.getStationStationId()))
                .forEach(failureInformationStationRepository::delete);

    }

    private void setRadius(List<TblFailureInformation> listFailureInfos, TblFailureInformation condensedFailureInformation){
            condensedFailureInformation.setRefRadius(
                    listFailureInfos.stream()
                            .map(TblFailureInformation::getRefRadius)
                            .filter(Objects::nonNull)
                            .max(Comparator.comparingLong(RefRadius::getRadius))
                                .orElse(null));
    }

    private void setAddress(List<TblFailureInformation> listFailureInfos, TblFailureInformation condensedFailureInformation){

        resetCondensedFailureInformationAdress(condensedFailureInformation);

        setAddressForCondensedFailureInformation(listFailureInfos, condensedFailureInformation);

    }

    private void resetCondensedFailureInformationAdress(TblFailureInformation condensedFailureInformation) {
        condensedFailureInformation.setCity(null);
        condensedFailureInformation.setDistrict(null);
        condensedFailureInformation.setPostcode(null);
        condensedFailureInformation.setStreet(null);
        condensedFailureInformation.setHousenumber(null);
        condensedFailureInformation.setLongitude(null);
        condensedFailureInformation.setLatitude(null);
        condensedFailureInformation.setAddressType(null);
    }

    private void setAddressForCondensedFailureInformation(List<TblFailureInformation> listFailureInfos,
                                                          TblFailureInformation condensedFailureInformation) {
        Optional<TblFailureInformation> tblInfTocheck =
                listFailureInfos.stream().filter(f -> (f.getCity() != null)).findFirst();
        if (tblInfTocheck.isPresent()) {
            long count =
                    listFailureInfos.stream().filter(p -> tblInfTocheck.get().getCity().equals(p.getCity())).count();
            if (count == listFailureInfos.size()) {
                condensedFailureInformation.setCity(tblInfTocheck.get().getCity());
                condensedFailureInformation.setDistrict(tblInfTocheck.get().getDistrict());
                condensedFailureInformation.setPostcode(tblInfTocheck.get().getPostcode());
                condensedFailureInformation.setStreet(tblInfTocheck.get().getStreet());
                condensedFailureInformation.setHousenumber(tblInfTocheck.get().getHousenumber());
                condensedFailureInformation.setLongitude(tblInfTocheck.get().getLongitude());
                condensedFailureInformation.setLatitude(tblInfTocheck.get().getLatitude());
                condensedFailureInformation.setAddressType(tblInfTocheck.get().getAddressType());
                condensedFailureInformation.setFaultLocationArea(tblInfTocheck.get().getFaultLocationArea());
            }
        }
        else {
            // There is not info in the list having at least a city
            // ... so try at least to merge the coordinates
            setCondensedFailureInformationCoordinates( listFailureInfos, condensedFailureInformation);
        }
    }


    //set first found coordinates if there is no consense at all
    private void setCondensedFailureInformationCoordinates(List<TblFailureInformation> listFailureInfos, TblFailureInformation condensedFailureInformation) {
        Optional<TblFailureInformation> firstFailureInformationFiltered = listFailureInfos
                .stream().filter(f -> (f.getLongitude() != null && f.getLatitude() != null )).findFirst();

        if (firstFailureInformationFiltered.isPresent()) {
            condensedFailureInformation.setLongitude(firstFailureInformationFiltered.get().getLongitude());
            condensedFailureInformation.setLatitude(firstFailureInformationFiltered.get().getLatitude());
            condensedFailureInformation.setFaultLocationArea(firstFailureInformationFiltered.get().getFaultLocationArea());
        }
    }


    private void setExpectedReason(List<TblFailureInformation> listFailureInfos, TblFailureInformation condensedFailureInformation){
        RefExpectedReason firstExpReason = listFailureInfos.get(0).getRefExpectedReason();
        UUID firstExpectedReason = firstExpReason != null ? firstExpReason.getUuid() : null;

        List<TblFailureInformation> filteredList = listFailureInfos
                .stream()
                .filter(f->  f.getRefExpectedReason() != null && f.getRefExpectedReason().getUuid()!=null)
                .filter(f -> f.getRefExpectedReason().getUuid().equals(firstExpectedReason))
                .toList();
        if (listFailureInfos.size() == filteredList.size()) {
            condensedFailureInformation.setRefExpectedReason(listFailureInfos.get(0).getRefExpectedReason());
        }
        else{
            condensedFailureInformation.setRefExpectedReason(null);
        }
    }

    private void setLatestEnddate(List<TblFailureInformation> listFailureInfos, TblFailureInformation condensedFailureInformation){

        Date latest  = listFailureInfos
                .stream()
                .map(TblFailureInformation::getFailureEndPlanned)
                .filter(Objects::nonNull).max(Comparator.naturalOrder())
                .orElse(null);

        condensedFailureInformation.setFailureEndPlanned(latest);
    }

    private void setEarliestStartdate(List<TblFailureInformation> listFailureInfos, TblFailureInformation condensedFailureInfo){

        Date earliest =  listFailureInfos
                .stream()
                .map(TblFailureInformation::getFailureBegin)
                .filter(Objects::nonNull).min(Comparator.naturalOrder())
                .orElse(null);

        condensedFailureInfo.setFailureBegin(earliest);
    }

    private boolean checkSameBranch(List<TblFailureInformation> listFailureInfos){
        RefBranch firstBranch = listFailureInfos.get(0).getRefBranch();
        List<TblFailureInformation> filteredList = listFailureInfos.stream().filter(f -> f.getRefBranch().equals(firstBranch)).toList();

        return listFailureInfos.size() == filteredList.size();
    }

    private void setVoltageLevel(List<TblFailureInformation> listFailureInfos, TblFailureInformation condensedFailureInfo){
        String firstVoltageLevel = listFailureInfos.get(0).getVoltageLevel();

        List<TblFailureInformation> filteredList = listFailureInfos
                .stream()
                .filter(f -> f.getVoltageLevel()!= null)
                .filter(f -> f.getVoltageLevel().equals(firstVoltageLevel))
                .toList();

        if (listFailureInfos.size() == filteredList.size()) {
            condensedFailureInfo.setVoltageLevel(firstVoltageLevel);
        }
        else {
            condensedFailureInfo.setVoltageLevel(null);
        }
    }

    private void addPolygonAddressPoints(FailureInformationDto failureInformationDto){
        // Stationen ermitteln
        List<UUID> stationUuids =
                failureInformationStationRepository.findUuidByFkTblFailureInformation(failureInformationDto.getUuid());

        if (stationUuids.isEmpty()) {
            log.trace("No station ids found for failureInfo: " + failureInformationDto.getUuid().toString());
            return;
        }

        List<StationPolygonDto> addressPolygonPoints = getPolygonCoordinatesForStationUuids(stationUuids);

        if (!addressPolygonPoints.isEmpty()) {
            failureInformationDto.setAddressPolygonPoints(addressPolygonPoints);
        }
    }

    public List<StationPolygonDto> getPolygonCoordinatesForStationUuids(List<UUID> stationUuids) {

        List<StationPolygonDto> stationPolygonDtoList = new ArrayList<>();

        // Holen der Adressen, die fuer die Stoerungsinformation relevant sind
        for (UUID stationUuid : stationUuids) {
            TblStation tblStation = stationRepository.findByUuid(stationUuid).orElse(null);

            if (tblStation == null) {
                log.warn("station " + stationUuid.toString() + "not found ");
                continue;
            }

            if (Strings.isNotBlank(tblStation.getGeoJson())) {
                processGeoJsonColumn(stationPolygonDtoList, stationUuid, tblStation);
                continue;
            }

            List<TblAddress> addressListForStation = addressRepository.findByStationId(tblStation.getStationId());
            if (addressListForStation.isEmpty()) {
                log.debug("Station " + stationUuid.toString() + "has no addresses.");
            } else {
                StationPolygonDto stationPolygonDto = createStationPolygonDto(stationUuid, addressListForStation);
                stationPolygonDtoList.add(stationPolygonDto);
            }

        }

        return stationPolygonDtoList;
    }

    private void processGeoJsonColumn(List<StationPolygonDto> stationPolygonDtoList, UUID stationUuid, TblStation tblStation) {
        StationGeoJson stationGeoJson;
        try {
            stationGeoJson = objectMapper.readValue(tblStation.getGeoJson(), StationGeoJson.class);
        } catch (JsonProcessingException e) {
            log.error("Error parsing GeoJson for Station (Polygons)", e);
            throw new InternalServerErrorException("Error parsing GeoJson for Station");
        }

        List<Feature> features = stationGeoJson.getFeatures();
        for (Feature polygon : features) {
            StationPolygonDto stationPolygonDto = createStationPolygonDto(stationUuid, polygon);
            stationPolygonDtoList.add(stationPolygonDto);
        }
    }

    private static StationPolygonDto createStationPolygonDto(UUID stationUuid, Feature polygon) {
        StationPolygonDto stationPolygonDto = new StationPolygonDto();
        stationPolygonDto.setStationId(stationUuid.toString());

        // Bei Type Poylgon: polygonStructureList[0] = äußeres Polygon
        // falls vorhanden wäre polygonStructureList[1] = inneres Polygon
        // https://de.wikipedia.org/wiki/GeoJSON
        List<List<List<BigDecimal>>> polygonStructureList = polygon.getGeometry().getCoordinates();
         if (polygonStructureList.get(0) != null) {
             List<List<BigDecimal>> outerPolygonGeoJson = polygonStructureList.get(0);
             //Transform Latitude und Longitude, SIT erwartet im FE lon,lat, der GEOJson Standard ist lat,lon.
             // Hinweis, die Menschheit kann sich aktuell nicht entscheiden: https://macwright.com/lonlat/
             List<List<BigDecimal>> outerPolygonSIT = transformLatLon(outerPolygonGeoJson);
             stationPolygonDto.setPolygonCoordinatesList(outerPolygonSIT);
         }
        return stationPolygonDto;
    }

    private static List<List<BigDecimal>> transformLatLon(List<List<BigDecimal>> outerPolygonGeoJson) {
        List<List<BigDecimal>> outerPolygonSIT = new ArrayList<>();
        for (List<BigDecimal> coordinateTuple : outerPolygonGeoJson) {
            List<BigDecimal> coordinateTupleSIT = new ArrayList<>();
            BigDecimal longitudeGeoJson = coordinateTuple.get(0);
            BigDecimal latitudeGeoJson = coordinateTuple.get(1);
            coordinateTupleSIT.add(latitudeGeoJson);
            coordinateTupleSIT.add(longitudeGeoJson);
            outerPolygonSIT.add(coordinateTupleSIT);
        }
        return outerPolygonSIT;
    }

    private static StationPolygonDto createStationPolygonDto(UUID stationUuid, List<TblAddress> addressListForStation) {
        StationPolygonDto stationPolygonDto = new StationPolygonDto();
        stationPolygonDto.setStationId(stationUuid.toString());
        List<List<BigDecimal>> stationPolygon = getStationPolygon(addressListForStation);
        stationPolygonDto.setPolygonCoordinatesList(stationPolygon);
        return stationPolygonDto;
    }

    private static List<List<BigDecimal>> getStationPolygon(List<TblAddress> allAdresses) {
        List<Point> addressPoints = new LinkedList<>();
        // Hinzufügen der gefundenen Adressen zur Liste der Adresspunkte
        // (Multiplikation mit 1000000 notwendig zur Erlangung von ganzzahligen Werten für die Übergabe an GrahamScan)
        allAdresses
                .forEach(x -> addressPoints.add
                        (new Point((x.getLatitude().setScale(6, RoundingMode.FLOOR).movePointRight(6)).intValue(),
                                (x.getLongitude().setScale(6, RoundingMode.FLOOR).movePointRight(6)).intValue())));

        List<List<BigDecimal>> addressPolygonPoints = new ArrayList<>();
        if (addressPoints.isEmpty()) {
            log.debug("No addresses for the given stations available");
            return addressPolygonPoints;
        }

        // GrahamScan über die Adresspunkte zum Erhalt der äußeren Punkte, die ein Polygon bilden
        List<Point> polygonPoints = GrahamScan.getConvexHull(addressPoints);

        // Verpacken der Polygonpunkte in eine Liste von Arrays (longitude/latitude) die ins DTO der Störungsmeldung geschrieben wird
        // (Division durch 1000000 notwendig zur Wiedererlangung der ursprünglichen Werte)
        for (Point point : polygonPoints) {
            ArrayList<BigDecimal> twoValues = new ArrayList<>();
            twoValues.add(BigDecimal.valueOf(point.getX()).movePointLeft(6));
            twoValues.add(BigDecimal.valueOf(point.getY()).movePointLeft(6));
            addressPolygonPoints.add(twoValues);
        }

        return addressPolygonPoints;
    }

    public FailureInformationPublicationChannelDto insertPublicationChannelForFailureInfo(UUID failureInfoUuid, String publicationChannel, boolean isPublished){
        TblFailureInformation existingTblFailureInformation = failureInformationRepository
                .findByUuid(failureInfoUuid)
                .orElseThrow(() -> new NotFoundException(Constants.FAILURE_INFO_UUID_NOT_EXISTING));

        //check if channel is already existing
        if(failureInformationPublicationChannelRepository.countByTblFailureInformationAndPublicationChannel(existingTblFailureInformation, publicationChannel)>0){
            throw new BadRequestException("channel.already.existing");
        }

        //create new channel
        TblFailureInformationPublicationChannel tfiPublicationChannelToSave = new TblFailureInformationPublicationChannel();
        tfiPublicationChannelToSave.setTblFailureInformation(existingTblFailureInformation);
        tfiPublicationChannelToSave.setPublicationChannel(publicationChannel);
        tfiPublicationChannelToSave.setPublished(isPublished);

        TblFailureInformationPublicationChannel savedTfiPublicationChannel = failureInformationPublicationChannelRepository.save(tfiPublicationChannelToSave);

        return failureInformationPublicationChannelMapper.toFailureInformationPublicationChannelDto(savedTfiPublicationChannel);
    }

    public void deletePublicationChannelForFailureInfo(UUID failureInfoUuid, String publicationChannel){

        TblFailureInformation existingTblFailureInformation = failureInformationRepository
                .findByUuid(failureInfoUuid)
                .orElseThrow(() -> new NotFoundException(Constants.FAILURE_INFO_UUID_NOT_EXISTING));

        TblFailureInformationPublicationChannel existingTblFailureInformationPublicationChannel = failureInformationPublicationChannelRepository
                .findByTblFailureInformationAndPublicationChannel(existingTblFailureInformation, publicationChannel)
                .orElseThrow(() -> new NotFoundException("channel.not.existing"));

        failureInformationPublicationChannelRepository.delete(existingTblFailureInformationPublicationChannel);

    }


    public List<FailureInformationPublicationChannelDto> getPublicationChannelsForFailureInfo(UUID failureInfoUuid){

        TblFailureInformation existingTblFailureInformation = failureInformationRepository
                .findByUuid(failureInfoUuid)
                .orElseThrow(() -> new NotFoundException(Constants.FAILURE_INFO_UUID_NOT_EXISTING));

        List<TblFailureInformationPublicationChannel> publicationChannels = failureInformationPublicationChannelRepository
                .findByTblFailureInformation(existingTblFailureInformation);

       return publicationChannels
                .stream()
                .map(failureInformationPublicationChannelMapper::toFailureInformationPublicationChannelDto)
                .collect(Collectors.toList());

    }

    @Transactional
    public void deleteFailureInfo(UUID failureInfoUuid){

        TblFailureInformation existingTblFailureInformation = failureInformationRepository
                .findByUuid(failureInfoUuid)
                .orElseThrow(() -> new NotFoundException("failure.info.uuid.not.existing"));

        if(isStatusNewPlannedCreated(existingTblFailureInformation)){

            if (existingTblFailureInformation.getTblFailureInformationCondensed() != null) {
                TblFailureInformation tblFailureInformationCondensed =
                        existingTblFailureInformation.getTblFailureInformationCondensed();

                Integer condensedCount = tblFailureInformationCondensed.getCondensedCount();
                tblFailureInformationCondensed.setCondensedCount(--condensedCount);
                failureInformationRepository.save(tblFailureInformationCondensed);
            }

            failureInformationPublicationChannelRepository.deleteAll(failureInformationPublicationChannelRepository.findByTblFailureInformation(existingTblFailureInformation));

            Optional<TblFailureInformationReminderMailSent> reminder = failureInformationReminderMailSentRepository.findByTblFailureInformation(existingTblFailureInformation);
            if(reminder.isPresent()) {
                failureInformationReminderMailSentRepository.deleteByTblFailureInformation(existingTblFailureInformation);
            }

            failureInformationStationRepository.deleteAll(failureInformationStationRepository.findByFailureInformationId(existingTblFailureInformation.getId()));

            failureInformationDistributionGroupRepository.deleteAll(failureInformationDistributionGroupRepository.findByFailureInformationId(existingTblFailureInformation.getId()));

            failureInformationRepository.findByFailureInformationCondensedUuid(existingTblFailureInformation.getUuid())
                    .forEach( this::undoCondensedOnParentDelete );
            failureInformationRepository.delete(existingTblFailureInformation);
        }
        else{
            throw new BadRequestException("delete.not.allowed");
        }
    }

    public FailureInformationLastModDto getFailureInformationLastModTimeStamp() {
        FailureInformationLastModDto lastModDto = new FailureInformationLastModDto();
        List<TblFailureInformation> visibleList = findFailureInformationsForDisplay(Pageable.unpaged()).toList();

        // get the newest Elem out of the list of visible elements
        TblFailureInformation newestElem = visibleList.stream().max(Comparator.comparing(TblFailureInformation::getModDate))
                .orElseThrow(() -> new NotFoundException("no.failures.available"));
        lastModDto.setLastModification(newestElem.getModDate());
        lastModDto.setUuid(newestElem.getUuid());

        return lastModDto;
      }

    public boolean isFailureInfoPlanned(FailureInformationDto dto) {
        RefFailureClassification refPlannedMeasure = failureClassificationRepository
                .findById(plannedMeasureDbId)
                .orElseThrow(() -> new InternalServerErrorException("planned.measure.obj.not.found"));
        return refPlannedMeasure.getUuid().equals(dto.getFailureClassificationId());
    }

    public List<String> getChannelsToPublishList(UUID uuid, boolean onlyMail) {
        TblFailureInformation tblFailureInfo = failureInformationRepository.findByUuid(uuid).orElseThrow(() -> new NotFoundException(""));
        List<TblFailureInformationPublicationChannel> channelList = failureInformationPublicationChannelRepository.findByTblFailureInformation(tblFailureInfo);
        List<String> finalChannelList = channelList.stream().map(TblFailureInformationPublicationChannel::getPublicationChannel).collect(Collectors.toList());

        if (onlyMail){
            //remove all but email && sms channel
            finalChannelList.removeIf(p -> !p.equals(rabbitMqConfig.getChannelTypeToNameMap().get(Constants.CHANNEL_TPYE_SHORT_MAIL))
                    && !p.equals(rabbitMqConfig.getChannelTypeToNameMap().get(Constants.CHANNEL_TPYE_LONG_MAIL)));
        }
        return finalChannelList;
    }

    private void undoCondensedOnParentDelete(TblFailureInformation x) {
        x.setTblFailureInformationCondensed(null);
        setVersionNumber(x);
        failureInformationRepository.save(x);
    }

    private boolean isStatusNewPlannedCreated(TblFailureInformation tblFailureInformation){
        RefStatus refStatus = tblFailureInformation.getRefStatusIntern();
        return (refStatus.getStatus().equals("neu") || refStatus.getStatus().equals("geplant") || refStatus.getStatus().equals("angelegt"));
    }
}
