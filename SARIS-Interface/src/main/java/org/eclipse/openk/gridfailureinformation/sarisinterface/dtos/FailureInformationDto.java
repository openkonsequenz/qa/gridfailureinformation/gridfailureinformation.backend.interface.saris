/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.sarisinterface.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class FailureInformationDto implements Serializable {
    @JsonProperty("id")
    private UUID uuid;
    private String title;
    private String description;
    private Long versionNumber;
    private String responsibility;
    private String internExtern;
    private String voltageLevel;
    private String pressureLevel;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureBegin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureEndPlanned;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureEndResupplied;

    private String postcode;
    private String city;
    private String district;
    private String street;
    private String housenumber;
    private String stationId;
    private String stationDescription;
    private String stationCoords;
    private BigDecimal longitude;
    private BigDecimal latitude;
    private String objectReferenceExternalSystem;
    private String publicationStatus;
    private String publicationFreetext;
    private Boolean condensed;
    private Long condensedCount;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date createDate;
    private String createUser;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date modDate;
    private String modUser;

    private UUID failureClassificationId;
    private String failureClassification;

    private UUID failureTypeId;
    private String failureType;

    private boolean isPlanned;

    private UUID statusInternId;
    private String statusIntern;

    private UUID statusExternId;
    private String statusExtern;

    private UUID branchId;
    private String branch;
    private String branchDescription;
    private String branchColorCode;

    private UUID radiusId;
    private Long radius;

    private UUID expectedReasonId;
    private String expectedReasonText;

    private UUID failureInformationCondensedId;

    // 0: Lat
    // 1: Lng
    private List<ArrayList<BigDecimal>> addressPolygonPoints;

    private List<UUID> distributionGroupUuids;

    private List<String> publicationChannels;

}
