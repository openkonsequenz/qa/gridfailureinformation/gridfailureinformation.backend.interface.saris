/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.controller;

import org.eclipse.openk.gridfailureinformation.samointerface.SarisInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.samointerface.service.ImportService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = SarisInterfaceApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ImportControllerTest {

    @MockBean
    private ImportService importService;

    @Autowired
    private MockMvc mockMvc;


//    @Test
//    public void shouldCallImport() throws Exception {
//        mockMvc.perform(get("/stoerungsauskunft/usernotification-import-test"))
//                .andExpect(status().is2xxSuccessful());
//
//        verify(importService, times(1)).importUserNotifications();
//    }
}
