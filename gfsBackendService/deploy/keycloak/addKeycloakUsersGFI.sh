#!/bin/sh
echo ------- Login Keycloak -------
sh kcadm.sh config credentials --server http://localhost:8380/auth --realm master --user admin --password admin
realmVar="OpenKRealm"
# ***************** CREATING NEW USER *****************
usernameVar="leonie_e"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Leonie -s lastName=Erfasser -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-creator -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="marie_q"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Marie -s lastName=Qualifizierer -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-qualifier -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="laura_p"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Laura -s lastName=Öffentlichkeitsarbeit -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-publisher -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="liam_e"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Liam -s lastName=Erfasser -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-creator -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="theo_q"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Theo -s lastName=Qualifizierer -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-qualifier -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="tom_p"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Tom -s lastName=Öffentlichkeitsarbeit -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-publisher -r $realmVar
echo roles set

echo ------- Finished -------
