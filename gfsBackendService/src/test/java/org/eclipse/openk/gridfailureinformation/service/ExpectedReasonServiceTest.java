/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefBranch;
import org.eclipse.openk.gridfailureinformation.model.RefExpectedReason;
import org.eclipse.openk.gridfailureinformation.repository.BranchRepository;
import org.eclipse.openk.gridfailureinformation.repository.ExpectedReasonRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.ExpectedReasonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class ExpectedReasonServiceTest {
    @Autowired
    private ExpectedReasonRepository expectedReasonRepository;

    @Autowired
    private ExpectedReasonService expectedReasonService;

    @Autowired
    private BranchRepository branchRepository;

    @Test
    public void shouldGetExpectedReasonsProperly() {
        List<RefExpectedReason> mockRefExpectedReasonList = MockDataHelper.mockRefExpectedReasonList();
        String branchName = mockRefExpectedReasonList.get(0).getBranches();
        Optional<String> branchOptional = Optional.of(branchName);

        when(expectedReasonRepository.findAll()).thenReturn(mockRefExpectedReasonList);
        when(expectedReasonRepository.findByBranch(eq(branchName))).thenReturn(mockRefExpectedReasonList);

        List<ExpectedReasonDto> expectedReasons = expectedReasonService.getExpectedReasons(branchOptional);

        assertEquals(mockRefExpectedReasonList.size(), expectedReasons.size());
        assertEquals(2, expectedReasons.size());
        assertEquals(expectedReasons.get(1).getUuid(), mockRefExpectedReasonList.get(1).getUuid());
    }

    @Test
    public void shouldGetExpectedReasonsForBranch() {
        Optional<String> branchOptional = Optional.of("S");
        RefBranch mockBranch = MockDataHelper.mockRefBranch();

        List<RefExpectedReason> mockRefExpectedReasonList = MockDataHelper.mockRefExpectedReasonList();

        when(branchRepository.findByName(any(String.class))).thenReturn(Optional.of(mockBranch));
        when(expectedReasonRepository.findByBranch(any(String.class))).thenReturn(mockRefExpectedReasonList);
        List<ExpectedReasonDto> expectedReasons = expectedReasonService.getExpectedReasons(branchOptional);

        assertEquals(expectedReasons.size(), mockRefExpectedReasonList.size());
        assertEquals(2, expectedReasons.size());
        assertEquals(expectedReasons.get(1).getUuid(), mockRefExpectedReasonList.get(1).getUuid());
    }
}
