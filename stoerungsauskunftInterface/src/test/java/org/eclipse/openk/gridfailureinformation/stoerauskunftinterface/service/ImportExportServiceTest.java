/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Request;
import feign.Response;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.StoerungsauskunftInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api.StoerungsauskunftApi;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.StoerungsauskunftUserNotification;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.support.MockDataHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = StoerungsauskunftInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class ImportExportServiceTest {
    @Autowired
    private ImportExportService importExportService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Response testResponse;

    @SpyBean
    private StoerungsauskunftApi stoerungsauskunftApi;

    @Test
    public void shouldImportUserNotification() throws IOException {
        ImportExportService importExportServicSpy = spy(importExportService);

        InputStream is = new ClassPathResource("UsernotificationJsonResponse.json").getInputStream();
        List<StoerungsauskunftUserNotification> userNotificationList =
                objectMapper.readValue(is, new TypeReference<>() {
                });
        when(stoerungsauskunftApi.getUserNotification(anyInt())).thenReturn(userNotificationList);

        importExportServicSpy.importUserNotifications();

        verify(importExportServicSpy, times(userNotificationList.size())).pushForeignFailure(any(ForeignFailureMessageDto.class));
    }

    @Test
    public void shouldExportForeignFailureMessageDto() {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        when(stoerungsauskunftApi.postOutage(anyList(), anyBoolean())).thenReturn(testResponse);

        importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto);

        verify(stoerungsauskunftApi, times(1)).postOutage(anyList(), anyBoolean());
    }

    @Test
    public void shouldExportForeignFailureMessageDto_planned_polygons() {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();
        rabbitMqMessageDto.getFailureInformationDto().setPlanned(true);
        rabbitMqMessageDto.getFailureInformationDto().setAddressPolygonPoints(MockDataHelper.mockPolygonCoordinatesList());

        Request request = mock(Request.class);
        Response mockResponse = Response.builder().status(200).request(request).body("Test", StandardCharsets.UTF_8).build();

        when(stoerungsauskunftApi.postOutage(anyList(), anyBoolean())).thenReturn(mockResponse);

        importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto);

        verify(stoerungsauskunftApi, times(1)).postOutage(anyList(), anyBoolean());
    }

    @Test
    public void shouldExportForeignFailureMessageDto_planned_polygons_empty() {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();
        rabbitMqMessageDto.getFailureInformationDto().setPlanned(true);
        rabbitMqMessageDto.getFailureInformationDto().setAddressPolygonPoints(new ArrayList<>());

        when(stoerungsauskunftApi.postOutage(anyList(), anyBoolean())).thenReturn(testResponse);

        importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto);

        verify(stoerungsauskunftApi, times(1)).postOutage(anyList(), anyBoolean());
    }

    @Test
    public void shouldThrowErrorWhenCallToExternalInterfaceFailed() {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        Request request = mock(Request.class);
        Response mockResponse = Response.builder().status(400).request(request).body("Test", StandardCharsets.UTF_8).build();

        when(stoerungsauskunftApi.postOutage(anyList(), anyBoolean())).thenReturn(mockResponse);

        assertThrows(InternalServerErrorException.class, () -> importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto));
    }

    @Test
    public void shouldImportUserNotificationMapperTest1() throws IOException {
        ImportExportService importExportServicSpy = spy(importExportService);

        InputStream is = new ClassPathResource("UsernotificationJsonResponse.json").getInputStream();
        List<StoerungsauskunftUserNotification> userNotificationList =
                objectMapper.readValue(is, new TypeReference<>() {
                });
        when(stoerungsauskunftApi.getUserNotification(anyInt())).thenReturn(userNotificationList);

        importExportServicSpy.importUserNotifications();

        verify(importExportServicSpy, times(userNotificationList.size())).pushForeignFailure(any(ForeignFailureMessageDto.class));
    }
}
