/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationDistributionGroupService;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDistributionGroupDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/grid-failure-informations")
public class FailureInformationDistributionGroupController {
    private final FailureInformationDistributionGroupService failureInformationDistributionGroupService;

    public FailureInformationDistributionGroupController(FailureInformationDistributionGroupService failureInformationDistributionGroupService) {
        this.failureInformationDistributionGroupService = failureInformationDistributionGroupService;
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @GetMapping("/{failureInfoUuid}/distribution-groups")
    @Operation(summary = "Anzeigen alle Verteilergruppen zu einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "404", description = "Verteilergruppe konnt nicht zugeordnet werden")})
    @ResponseStatus(HttpStatus.OK)
    public List<DistributionGroupDto> getDistributionGroupsForFailureInformation(@PathVariable UUID failureInfoUuid) {
        return failureInformationDistributionGroupService.findDistributionGroupsByFailureInfo(failureInfoUuid);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @PostMapping("/{failureInfoUuid}/distribution-groups")
    @Operation(summary = "Zuordnen einer Verteilergruppe zu einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Verteilergruppe erfolgreich zugeordnet"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<FailureInformationDistributionGroupDto> insertFailureInfoDistributionGroup(
            @PathVariable UUID failureInfoUuid,
            @Validated @RequestBody DistributionGroupDto dgDto) {

        FailureInformationDistributionGroupDto savedGfDgDto = failureInformationDistributionGroupService.insertFailureInfoDistributionGroup(failureInfoUuid, dgDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{groupid}")
                .buildAndExpand(savedGfDgDto.getDistributionGroupId())
                .toUri();
        return ResponseEntity.created(location).body(savedGfDgDto);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @DeleteMapping("/{failureInfoUuid}/distribution-groups/{groupUuid}")
    @Operation(summary = "Löschen der Zuordnung einer Verteilergruppe zu einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Verteilergruppenzuordnung erfolgreich gelöscht"),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden")
    })
    public void deleteFailureInfoDistributionGroup(
            @PathVariable UUID failureInfoUuid,
            @PathVariable UUID groupUuid) {
        failureInformationDistributionGroupService.deleteFailureInfoDistributionGroup(failureInfoUuid, groupUuid);
    }

}
