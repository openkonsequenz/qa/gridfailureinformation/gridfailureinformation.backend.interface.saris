/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.importadresses.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.AddressMapper;
import org.eclipse.openk.gridfailureinformation.importadresses.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.importadresses.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.AddressDto;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;


@Log4j2
@Service
public class AddressService {
    private final AddressRepository addressRepository;

    private final AddressMapper addressMapper;

    public AddressService(AddressRepository addressRepository, AddressMapper addressMapper) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
    }

    public AddressDto insertAddress(AddressDto addressDto){
        TblAddress tblAddressToSave = addressMapper.toTableAddress(addressDto);
        tblAddressToSave.setUuid(UUID.randomUUID());
        TblAddress tblSavedAddress = addressRepository.save(tblAddressToSave);
        return addressMapper.toAddressDto(tblSavedAddress);
    }

    public AddressDto updateOrInsertAddressByG3efid(AddressDto addressDto) {

        try {
            Optional<TblAddress> optTblAddress = addressRepository.findByG3efid(addressDto.getG3efid());

            if (optTblAddress.isPresent() ) {
                TblAddress tblAddressToSave =  optTblAddress.get();

                if (addressDto.isPowerConnection()) {
                    tblAddressToSave.setPowerConnection(true);
                    tblAddressToSave.setStationId(addressDto.getStationId());
                }
                if (addressDto.isWaterConnection()) {
                    tblAddressToSave.setWaterConnection(true);
                    tblAddressToSave.setWaterGroup(addressDto.getWaterGroup());
                }
                if (addressDto.isGasConnection()) {
                    tblAddressToSave.setGasConnection(true);
                    tblAddressToSave.setGasGroup(addressDto.getGasGroup());
                }
                if (addressDto.isDistrictheatingConnection()) {
                    tblAddressToSave.setDistrictheatingConnection(true);
                }
                if (addressDto.isTelecommConnection()) {
                    tblAddressToSave.setTelecommConnection(true);
                }

                TblAddress savedAddress = addressRepository.save(tblAddressToSave);
                return addressMapper.toAddressDto(savedAddress);
            } else {
                return insertAddress(addressDto);
            }

        } catch (Exception ex) {

            log.error(ex);
            log.warn("Adresse [sdo_x1= " + addressDto.getSdox1()
                    + ", sdo_y1= " + addressDto.getSdoy1()
                    + ", g3e_fid=" + addressDto.getG3efid()
                    + ", plz=" + addressDto.getPostcode()
                    + ", ort=" + addressDto.getCommunity()
                    + ", strasse=" + addressDto.getStreet()
                    + ", hausnummer=" + addressDto.getHousenumber()
                    + "]");

            return addressDto;
        }

    }

    public  void deleteAllAddresses() {
        addressRepository.deleteAllInBatch();
    }
}
