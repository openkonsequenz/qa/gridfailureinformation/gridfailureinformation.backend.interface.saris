/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.api.ContactApi;
import org.eclipse.openk.gridfailureinformation.api.dto.VwDetailedContact;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactService {
    @Value("${services.contacts.useModuleNameForFilter}")
    private boolean useModuleName;

    @Value("${services.contacts.moduleName}")
    private String contactModuleName;

    @Lazy
    private final ContactApi contactApi;

    public ContactService(ContactApi contactApi) {
        this.contactApi = contactApi;
    }

    public Page<VwDetailedContact> findContacts(Optional<String> searchText, Pageable pageable) {

        String jwt = (String) SecurityContextHolder.getContext().getAuthentication().getDetails();

        String st = searchText.orElse(null);
        String moduleName = null;

        if (useModuleName && !contactModuleName.isEmpty()) moduleName = contactModuleName;

        return contactApi.findContacts(st, moduleName, pageable, jwt );
    }
}
