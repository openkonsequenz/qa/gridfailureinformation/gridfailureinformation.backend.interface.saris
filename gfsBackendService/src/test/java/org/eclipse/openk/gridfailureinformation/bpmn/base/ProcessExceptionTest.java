/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProcessExceptionTest {
    @Test
    public void testExceptionCreationForCoverage() {
        new ProcessException("testOnly");
        Exception innerException = new ProcessException("inner Exception");
        Exception exc = new ProcessException("with Exception", innerException);
        assertEquals( exc.getCause(), innerException);

    }
}
