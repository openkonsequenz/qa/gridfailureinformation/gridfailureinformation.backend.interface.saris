/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class ForeignFailureDataDto implements Serializable {

    private boolean isAutopublish;

    private boolean isOnceOnlyImport;

    private boolean isExcludeEquals;

    private boolean isExcludeAlreadyEdited;

    @NotNull
    private boolean isPlanned;

    @JsonProperty("description")
    private String description;

    @NotNull
    @Pattern(regexp = "^$|(S|W|F|G|TK|ST|OS)")
    private String branch;

    @Pattern(regexp="^$|(NS|MS|HS)")
    private String voltageLevel;

    @Pattern(regexp="^$|(ND|MD|HD)")
    private String pressureLevel;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureBegin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureEndPlanned;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureEndResupplied;

    @Size(max=10)
    @Pattern(regexp="^$|[A-Za-z0-9_/-]+")
    private String postcode;

    @Size(max=200)
    @Pattern(regexp="^$|[A-Za-z0-9ÄäÖöÜüß\\s_/()-]+")
    private String city;

    @Size(max=200)
    @Pattern(regexp="^$|[A-Za-z0-9ÄäÖöÜüß\\s_/()-]+")
    private String district;

    @Size(max=200)
    @Pattern(regexp="^$|[A-Za-z0-9ÄäÖöÜüß\\s_/()-]+")
    private String street;

    @Size(max=10)
    @Pattern(regexp="^$|[A-Za-z0-9ÄäÖöÜüß\\s_/()-]+")
    private String housenumber;

    @Size(max=200)
    @Pattern(regexp="^$|[A-Za-z0-9ÄäÖöÜüß\\s_/()-]+")
    private String stationId;

    @Size(max=200)
    @Pattern(regexp="^$|[A-Za-z0-9ÄäÖöÜüß\\s_/(),.;:-]+")
    private String stationDescription;

    private BigDecimal longitude;
    private BigDecimal latitude;

    @Min(0L)
    @Max(100000L)
    private Long radiusInMeters;
}
