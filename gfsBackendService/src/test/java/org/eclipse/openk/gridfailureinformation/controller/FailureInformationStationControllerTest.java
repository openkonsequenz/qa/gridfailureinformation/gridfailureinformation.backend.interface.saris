/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationStationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationStationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class FailureInformationStationControllerTest {
    @Autowired
    private FailureInformationRepository failureInformationRepository;

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private FailureInformationStationRepository failureInformationStationRepository;

    @Autowired
    @SpyBean
    private FailureInformationStationService failureInformationStationService;

    private static MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new FailureInformationStationController(failureInformationStationService))
                .build();
    }

    @Test
    public void shouldReturnStationsForFailureInfo() throws Exception {
        List<StationDto> stationDtoList = MockDataHelper.mockStationDtoList();

        doReturn(stationDtoList).when(failureInformationStationService).findStationsByFailureInfo(any(UUID.class));

        mockMvc.perform(get("/grid-failure-informations/" + UUID.randomUUID() + "/stations"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldAssignStationToFailureInfo() throws Exception {
        FailureInformationStationDto failureInfoStationDto = MockDataHelper.mockFailureInformationStationDto();

        doReturn(failureInfoStationDto).when(failureInformationStationService).insertFailureInfoStation(any(UUID.class), any(StationDto.class));

        mockMvc.perform(post("/grid-failure-informations/" + UUID.randomUUID() + "/stations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(failureInfoStationDto)))
                .andExpect(jsonPath("failureInformationId", is(failureInfoStationDto.getFailureInformationId().intValue())))
                .andExpect(jsonPath("stationStationId", is(failureInfoStationDto.getStationStationId())))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldDeleteStationForFailureInformation() throws Exception {
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblStation tblStation = MockDataHelper.mockTblStation();
        TblFailinfoStation tblFailinfoStation = MockDataHelper.mockTblFailureInformationStation();

        when(failureInformationRepository.findByUuid(any())).thenReturn(Optional.of(tblFailureInformation));
        when(stationRepository.findByUuid(any())).thenReturn(Optional.of(tblStation));
        when(failureInformationStationRepository.findByFailureInformationIdAndStationStationId(any(), any())).thenReturn(Optional.of(tblFailinfoStation));

        mockMvc.perform(delete("/grid-failure-informations/" + UUID.randomUUID() + "/stations/" + UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }
}
