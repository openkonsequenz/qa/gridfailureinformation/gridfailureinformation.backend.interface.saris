/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mailexport.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.mailexport.MailExportApplication;
import org.eclipse.openk.gridfailureinformation.mailexport.controller.MessageConsumer;
import org.eclipse.openk.gridfailureinformation.mailexport.service.EmailService;
import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@EntityScan(basePackageClasses = MailExportApplication.class)
@ContextConfiguration(initializers = {ConfigDataApplicationContextInitializer.class})
@org.springframework.boot.test.context.TestConfiguration
@TestPropertySource(locations = "classpath:application-test.yml")
public class TestConfiguration {
    @Bean
    public ObjectMapper objectMapper() { return new ObjectMapper(); }

    @Bean
    public EmailConfig emailConfig() {
        return new EmailConfig();
    }

    @Bean
    public EmailService emailService() {
        return new EmailService(emailConfig());
    }

    @Bean
    public MessageChannel mailExportChannel() {return Mockito.mock(MessageChannel.class);}

    @Bean
    public MessageConsumer messageConsumer() {
        return new MessageConsumer(emailService(), objectMapper());
    }
}
