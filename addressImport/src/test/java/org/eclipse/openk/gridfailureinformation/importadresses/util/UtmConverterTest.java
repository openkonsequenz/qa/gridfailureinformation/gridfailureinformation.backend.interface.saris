/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.util;

import org.eclipse.openk.gridfailureinformation.importadresses.config.TestConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class UtmConverterTest {
    @Autowired
    private UtmConverter converter;

    @Test
    public void shouldInsertStationProperly() {
        List<BigDecimal> longLatList = converter.convertUTMToDeg(512959.19, 5400770.55);

        assertEquals(2, longLatList.size());
        assertEquals(new BigDecimal("9.176331"), longLatList.get(0));
        assertEquals(new BigDecimal("48.759810"), longLatList.get(1));
    }
}
