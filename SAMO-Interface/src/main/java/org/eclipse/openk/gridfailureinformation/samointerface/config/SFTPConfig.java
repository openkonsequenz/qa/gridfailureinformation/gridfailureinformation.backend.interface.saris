/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.config;

import com.jcraft.jsch.ChannelSftp;
import lombok.extern.log4j.Log4j2;
import org.apache.sshd.sftp.client.SftpClient;
import org.eclipse.openk.gridfailureinformation.samointerface.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileHeaders;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.handler.advice.ExpressionEvaluatingRequestHandlerAdvice;
import org.springframework.integration.sftp.filters.SftpSimplePatternFileListFilter;
import org.springframework.integration.sftp.inbound.SftpStreamingMessageSource;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpFileInfo;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;
import org.springframework.integration.transformer.StreamTransformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import java.io.InputStream;

@Log4j2
@Configuration
@ConditionalOnProperty(prefix = "sftp", name = "enable-polling", havingValue = "true", matchIfMissing = false)
public class SFTPConfig {
    private final ImportService importService;

    @Value("${sftp.host:}")
    public String sftpHost;
    @Value("${sftp.port:22}")
    public int sftpPort;
    @Value("${sftp.user:}")
    public String sftpUser;
    @Value("${sftp.password:}")
    public String sftpPwd;

    @Value("${sftp.privateKey:#{null}}")
    private Resource sftpPrivateKey;
    @Value("${sftp.privateKeyPassphrase:}")
    private String sftpPrivateKeyPassphrase;

    @Value("${sftp.directory:}")
    public String sftDirectory;
    @Value("${sftp.fileFilter:*.*}")
    public String sftpFileFilter;
    @Value("${sftp.deleteRemoteFile:false}")
    public boolean deleteRemoteFile;

    private long remoteFileLastModified = 0;

    public SFTPConfig(ImportService importService) {
        this.importService = importService;
    }

    @Bean
    public SessionFactory<SftpClient.DirEntry> sftpSessionFactory() {
        DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory(true);
        factory.setHost(sftpHost);
        factory.setPort(sftpPort);
        factory.setUser(sftpUser);
        if (sftpPrivateKey != null) {
            log.info("Using privatekey for SSH-Connection");
            factory.setPrivateKey(sftpPrivateKey);
            factory.setPrivateKeyPassphrase(sftpPrivateKeyPassphrase);
        } else {
            log.info("Using User/Password for SSH-Connection");
            factory.setPassword(sftpPwd);
        }

        factory.setAllowUnknownKeys(true);
        return new CachingSessionFactory<>(factory);
    }

    @Bean
    @InboundChannelAdapter(channel = "stream", poller = @Poller(cron = "${sftp.cron}"))
    public MessageSource<InputStream> sftpMessageSource() {
        SftpStreamingMessageSource messageSource = new SftpStreamingMessageSource(template());
        String remoteDirecotry = sftDirectory;
        messageSource.setRemoteDirectory(remoteDirecotry);
        messageSource.setFilter(new SftpSimplePatternFileListFilter(sftpFileFilter));
        messageSource.setFileInfoJson(false);
        return messageSource;
    }

    @Bean
    public SftpRemoteFileTemplate template() {
        return new SftpRemoteFileTemplate(sftpSessionFactory());
    }

    @Bean
    @Transformer(inputChannel = "stream", outputChannel = "data")
    public org.springframework.integration.transformer.Transformer transformer() {
        return new StreamTransformer("UTF-8");
    }


    @ServiceActivator(inputChannel = "data", adviceChain = "afterChain")
    @Bean
    public MessageHandler handler() {

        return new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException {

                if (isFileModified(message)) {
                    log.debug("File modified or new: "+ message.getHeaders().get(FileHeaders.REMOTE_FILE));
                    String payload = (String) message.getPayload();
                    importService.importSAMOOutage(payload);
                }
            }
        };
    }

    private boolean isFileModified(Message<?> message) {
        SftpFileInfo remoteFileInfo = (SftpFileInfo) message.getHeaders().get(FileHeaders.REMOTE_FILE_INFO);
        long modified = 0;
        if (remoteFileInfo != null) {
            modified = remoteFileInfo.getModified();
        }
        if (modified != remoteFileLastModified && modified != 0) {
            remoteFileLastModified = modified;
            return true;
        }
        return false;
    }

    @Bean
    public ExpressionEvaluatingRequestHandlerAdvice afterChain() {

        ExpressionEvaluatingRequestHandlerAdvice advice = new ExpressionEvaluatingRequestHandlerAdvice();
        if (deleteRemoteFile) {
            advice.setOnSuccessExpressionString("@template.remove(headers['file_remoteDirectory'] + headers['file_remoteFile'])");
        }
        advice.setPropagateEvaluationFailures(true);
        return advice;
    }
}


