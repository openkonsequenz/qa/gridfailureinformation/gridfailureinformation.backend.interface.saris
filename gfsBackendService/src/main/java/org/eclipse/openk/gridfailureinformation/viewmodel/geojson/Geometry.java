package org.eclipse.openk.gridfailureinformation.viewmodel.geojson;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
@Data
public class Geometry {

    private String type;
    private List<List<List<BigDecimal>>> coordinates;

}