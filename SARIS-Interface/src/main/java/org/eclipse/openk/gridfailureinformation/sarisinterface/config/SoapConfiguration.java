
package org.eclipse.openk.gridfailureinformation.sarisinterface.config;

import org.eclipse.openk.gridfailureinformation.sarisinterface.service.SarisWebservice;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class SoapConfiguration {
	@Value("${saris.apiUrl}")
	public String sarisApiUrl;

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in pom.xml
		marshaller.setContextPath("org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl");
		return marshaller;
	}

	@Bean
	public SarisWebservice sarisWebservice(Jaxb2Marshaller marshaller) {
		SarisWebservice sarisWebservice = new SarisWebservice();
		sarisWebservice.setDefaultUri(sarisApiUrl);
		sarisWebservice.setMarshaller(marshaller);
		sarisWebservice.setUnmarshaller(marshaller);
		return sarisWebservice;
	}
}
