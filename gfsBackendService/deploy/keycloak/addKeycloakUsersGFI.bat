echo ------- Login Keycloak -------
call kcadm config credentials --server http://localhost:8080/auth --realm master --user admin --password admin
REM ***************** CREATING NEW USER *****************
SET usernameVar=leonie_e
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Leonie -s lastName=Erfasser  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-creator -r Elogbook
echo roles set
REM ***************** CREATING NEW USER *****************
SET usernameVar=marie_q
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Marie -s lastName=Qualifizierer  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-qualifier -r Elogbook
echo roles set
REM ***************** CREATING NEW USER *****************
SET usernameVar=laura_p
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Laura -s lastName=Öffentlichkeitsarbeit  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-publisher -r Elogbook
echo roles set
REM ***************** CREATING NEW USER *****************
SET usernameVar=liam_e
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Liam -s lastName=Erfasser  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-creator -r Elogbook
echo roles set
REM ***************** CREATING NEW USER *****************
SET usernameVar=theo_q
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Theo -s lastName=Qualifizierer  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-qualifier -r Elogbook
echo roles set
REM ***************** CREATING NEW USER *****************
SET usernameVar=tom_p
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Tom -s lastName=Öffentlichkeitsarbeit  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-publisher -r Elogbook
echo roles set

echo ------- Finished -------
