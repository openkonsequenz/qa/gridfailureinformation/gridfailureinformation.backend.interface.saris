/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;


import org.eclipse.openk.gridfailureinformation.mapper.FailureClassificationMapper;
import org.eclipse.openk.gridfailureinformation.repository.FailureClassificationRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureClassificationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class FailureClassificationService {
    private final FailureClassificationRepository failureClassificationRepository;
    private final FailureClassificationMapper failureClassificationMapper;

    public FailureClassificationService(FailureClassificationRepository failureClassificationRepository, FailureClassificationMapper failureClassificationMapper) {
        this.failureClassificationRepository = failureClassificationRepository;
        this.failureClassificationMapper = failureClassificationMapper;
    }

    public List<FailureClassificationDto> getFailureClassifications() {
        return failureClassificationRepository.findAll().stream()
                .map( failureClassificationMapper::toFailureClassificationDto )
                .collect(Collectors.toList());
    }
}
