/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.support;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqChannel;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformationStation;
import org.eclipse.openk.gridfailureinformation.model.RefBranch;
import org.eclipse.openk.gridfailureinformation.model.RefExpectedReason;
import org.eclipse.openk.gridfailureinformation.model.RefFailureClassification;
import org.eclipse.openk.gridfailureinformation.model.RefRadius;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroupMember;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationPublicationChannel;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationReminderMailSent;
import org.eclipse.openk.gridfailureinformation.model.TblImportData;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.model.Version;
import org.eclipse.openk.gridfailureinformation.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.BranchDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.ExpectedReasonDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureClassificationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDistributionGroupDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationLastModDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationPublicationChannelDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationStationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.HistFailureInformationStationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.HousenumberUuidDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.ImportDataDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.RadiusDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationPolygonDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StatusDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.VersionDto;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
public class MockDataHelper {

    private MockDataHelper() {}

    public static Version mockVersion() {
        Version version = new Version();
        version.setId(4711L);
        version.setVersion("db-version_mock_1");
        return version;
    }

    public static VersionDto mockVersionDto() {
        VersionDto versionDto = new VersionDto();
        versionDto.setBackendVersion("660");
        versionDto.setDbVersion("550");
        return versionDto;
    }

    public static FailureInformationDto mockFailureInformationDto() {
        FailureInformationDto dto = new FailureInformationDto();
        dto.setUuid(UUID.randomUUID());
        dto.setStatusInternId(UUID.randomUUID());
        dto.setVersionNumber(3L);
        dto.setResponsibility("Vatter Abraham");
        dto.setVoltageLevel(Constants.VOLTAGE_LEVEL_MS);
        dto.setPressureLevel(Constants.PRESSURE_LEVEL_HD);
        dto.setFailureBegin(new java.util.Date(Date.valueOf("2022-12-01").getTime()));
        dto.setFailureEndPlanned(new java.util.Date(Date.valueOf("2022-12-02").getTime()));
        dto.setFailureEndResupplied(new java.util.Date(Date.valueOf("2022-12-03").getTime()));

        dto.setStreet("Budenweg");
        dto.setDistrict("West");
        dto.setCity("Waldau");
        dto.setStreet("Dieselstr.");
        dto.setHousenumber("54");
        dto.setPostcode("71111");

        dto.setStationId("224488-123bcd");
        dto.setStationDescription("Trafo 25");
        dto.setStationCoords("121,8855");
        dto.setLongitude(BigDecimal.valueOf(8.646280));
        dto.setLatitude(BigDecimal.valueOf(50.115618));

        dto.setRadiusId(UUID.randomUUID());
        dto.setRadius(50L);

        dto.setPublicationStatus("veröffentlicht");
        dto.setPublicationFreetext("Kabel aus Steckdose gerissen");

        dto.setExpectedReasonId(UUID.randomUUID());
        dto.setExpectedReasonText("Kabelfehler Niederspannung");

        dto.setFailureClassificationId(UUID.randomUUID());
        dto.setFailureClassification("FailClazz");

        dto.setFailureTypeId(UUID.randomUUID());
        dto.setFailureType("FailTypo");

        dto.setStatusInternId(UUID.randomUUID());
        dto.setStatusIntern("NEW");

        dto.setBranchId(UUID.randomUUID());
        dto.setBranch("G");
        dto.setBranchColorCode("#fdea64");

        dto.setFailureInformationCondensedId(UUID.randomUUID());

        dto.setCreateDate(new java.util.Date(Date.valueOf("2020-05-08").getTime()));
        dto.setCreateUser("weizenkeimk");
        dto.setModDate(new java.util.Date(Date.valueOf("2020-05-23").getTime()));
        dto.setModUser("schlonzh");

        dto.setStationIds(mockUuidList());
        return dto;
    }

    public static TblFailureInformation mockTblFailureInformation() {
        TblFailureInformation obj = new TblFailureInformation();
        obj.setId(22L);
        obj.setUuid(UUID.randomUUID());
        obj.setVersionNumber(4L);
        obj.setResponsibility("Vatter Abraham");
        obj.setVoltageLevel(Constants.VOLTAGE_LEVEL_MS);
        obj.setPressureLevel(Constants.PRESSURE_LEVEL_HD);

        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date date1 = dateformat.parse("01/12/2022");
            java.util.Date date2 = dateformat.parse("02/12/2022");
            java.util.Date date3 = dateformat.parse("03/12/2022");

            obj.setFailureBegin(date1);
            obj.setFailureEndPlanned(date2);
            obj.setFailureEndResupplied(date3);
        }
        catch(Exception e){
            log.warn("Fehler beim Erstellen von Daten mit DateFormat");
        }

        obj.setStreet("Budenweg");
        obj.setDistrict("West");
        obj.setCity("Waldau");
        obj.setStreet("Dieselstr.");
        obj.setHousenumber("54");
        obj.setPostcode("71111");

        obj.setStationId("224488-123bcd");
        obj.setStationDescription("Trafo 25");
        obj.setStationCoords("121,8855");
        obj.setLongitude(BigDecimal.valueOf(8.646280));
        obj.setLatitude(BigDecimal.valueOf(50.115618));

        obj.setRefRadius(mockRefRadius());

        obj.setPublicationStatus("veröffentlicht");
        obj.setPublicationFreetext("Kabel aus Steckdose gerissen");
        obj.setRefExpectedReason(mockRefExpectedReason());

        obj.setCreateDate(new java.util.Date(Date.valueOf("2020-05-08").getTime()));
        obj.setCreateUser("weizenkeimk");
        obj.setModDate(new java.util.Date(Date.valueOf("2020-05-23").getTime()));
        obj.setModUser("schlonzh");

        obj.setRefStatusIntern(mockRefStatusCreated());
        obj.setRefFailureClassification(mockRefFailureClassification());
        obj.setRefBranch(mockRefBranch());

        obj.setStations(new ArrayList<>(mockTblStationList()));

        return obj;
    }

    public static TblFailureInformation mockTblFailureInformation2() {
        TblFailureInformation obj = new TblFailureInformation();
        obj.setId(42L);
        obj.setUuid(UUID.randomUUID());
        obj.setVersionNumber(2L);
        obj.setResponsibility("Schlumpfine");
        obj.setVoltageLevel(Constants.VOLTAGE_LEVEL_HS);
        obj.setPressureLevel(Constants.PRESSURE_LEVEL_MD);

        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date date1 = dateformat.parse("12/08/2022");
            java.util.Date date2 = dateformat.parse("02/09/2022");
            java.util.Date date3 = dateformat.parse("02/09/2022");

            obj.setFailureBegin(date1);
            obj.setFailureEndPlanned(date2);
            obj.setFailureEndResupplied(date3);
        }
        catch(Exception e){
            log.warn("Fehler beim Erstellen von Daten mit DateFormat");
        }

        obj.setStreet("Hügelpfad");
        obj.setDistrict("Süd");
        obj.setCity("Schlumpfhausen");
        obj.setStreet("Benzinstr.");
        obj.setHousenumber("1");
        obj.setPostcode("81111");

        obj.setStationId("44444-123bcd");
        obj.setStationDescription("Trafo 11");
        obj.setStationCoords("101,2222");
        obj.setLongitude(BigDecimal.valueOf(12.646280));
        obj.setLatitude(BigDecimal.valueOf(51.115618));

        obj.setRefRadius(mockRefRadius());

        obj.setPublicationStatus("neu");
        obj.setPublicationFreetext("Korrodiertes Kabel");
        obj.setRefExpectedReason(mockRefExpectedReason());

        obj.setCreateDate(new java.util.Date(Date.valueOf("2021-05-11").getTime()));
        obj.setCreateUser("roggensackl");
        obj.setModDate(new java.util.Date(Date.valueOf("2020-05-12").getTime()));
        obj.setModUser("müllers");

        obj.setRefStatusIntern(mockRefStatus2());
        obj.setRefFailureClassification(mockRefFailureClassification());
        obj.setRefBranch(mockRefBranch());

        obj.setRefStatusIntern(mockRefStatusCreated());
        obj.setStations(mockTblStationList().stream().collect(Collectors.toList()));
        return obj;
    }

    public static TblFailureInformation mockTblFailureInformation3() {
        TblFailureInformation obj = new TblFailureInformation();
        obj.setId(22L);
        obj.setUuid(UUID.randomUUID());
        obj.setVersionNumber(4L);
        obj.setResponsibility("Vatter Mousa");
        obj.setVoltageLevel(Constants.VOLTAGE_LEVEL_NS);
        obj.setPressureLevel(Constants.PRESSURE_LEVEL_MD);

        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date date1 = dateformat.parse("04/12/2022");
            java.util.Date date2 = dateformat.parse("05/12/2022");
            java.util.Date date3 = dateformat.parse("06/12/2022");

            obj.setFailureBegin(date1);
            obj.setFailureEndPlanned(date2);
            obj.setFailureEndResupplied(date3);
        }
        catch(Exception e){
            log.warn("Fehler beim Erstellen von Daten mit DateFormat");
        }

        obj.setStreet("Budenweg");
        obj.setDistrict("West");
        obj.setCity("Waldau");
        obj.setStreet("Dieselstr.");
        obj.setHousenumber("54");
        obj.setPostcode("71111");

        obj.setStationId("224488-123bcd");
        obj.setStationDescription("Trafo 25");
        obj.setStationCoords("121,8855");
        obj.setLongitude(BigDecimal.valueOf(8.646280));
        obj.setLatitude(BigDecimal.valueOf(50.115618));

        obj.setRefRadius(mockRefRadius());

        obj.setPublicationStatus("veröffentlicht");
        obj.setPublicationFreetext("Kabel aus Steckdose gerissen");
        obj.setRefExpectedReason(mockRefExpectedReason());

        obj.setCreateDate(new java.util.Date(Date.valueOf("2020-05-08").getTime()));
        obj.setCreateUser("weizenkeimk");
        obj.setModDate(new java.util.Date(Date.valueOf("2020-05-23").getTime()));
        obj.setModUser("schlonzh");

        obj.setRefStatusIntern(mockRefStatusCreated());
        obj.setRefFailureClassification(mockRefFailureClassification());
        obj.setRefBranch(mockRefBranch());

        obj.setStations(mockTblStationList().stream().collect(Collectors.toList()));

        return obj;
    }

    public static TblFailureInformation mockTblFailureInformationWithoutAddress() {
        TblFailureInformation obj = new TblFailureInformation();
        obj.setId(42L);
        obj.setUuid(UUID.randomUUID());
        obj.setVersionNumber(2L);
        obj.setResponsibility("Schlumpfine");
        obj.setVoltageLevel(Constants.VOLTAGE_LEVEL_HS);
        obj.setPressureLevel(Constants.PRESSURE_LEVEL_MD);

        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date date1 = dateformat.parse("12/08/2022");
            java.util.Date date2 = dateformat.parse("02/09/2022");
            java.util.Date date3 = dateformat.parse("02/09/2022");

            obj.setFailureBegin(date1);
            obj.setFailureEndPlanned(date2);
            obj.setFailureEndResupplied(date3);
        }
        catch(Exception e){
            log.warn("Fehler beim Erstellen von Daten mit DateFormat");
        }

        obj.setStationId("44444-123bcd");
        obj.setStationDescription("Trafo 11");
        obj.setStationCoords("101,2222");
        obj.setLongitude(BigDecimal.valueOf(12.646280));
        obj.setLatitude(BigDecimal.valueOf(51.115618));

        obj.setRefRadius(mockRefRadius());

        obj.setPublicationStatus("neu");
        obj.setPublicationFreetext("Korrodiertes Kabel");
        obj.setRefExpectedReason(mockRefExpectedReason());

        obj.setCreateDate(new java.util.Date(Date.valueOf("2021-05-11").getTime()));
        obj.setCreateUser("roggensackl");
        obj.setModDate(new java.util.Date(Date.valueOf("2020-05-12").getTime()));
        obj.setModUser("müllers");

        obj.setRefStatusIntern(mockRefStatus2());
        obj.setRefFailureClassification(mockRefFailureClassification());
        obj.setRefBranch(mockRefBranch());

        obj.setRefStatusIntern(mockRefStatusCreated());
        obj.setStations(mockTblStationList().stream().collect(Collectors.toList()));
        return obj;
    }


    public static  Page<TblFailureInformation> mockTblFailureInformationPage() {
        List<TblFailureInformation> retList = new LinkedList<>();
        retList.add( mockTblFailureInformation() );
        retList.add( mockTblFailureInformation() );
        retList.get(1).setResponsibility("Lionel Lümmelprinz");
        return new PageImpl<>(retList, Pageable.unpaged(), retList.size());
    }


    public static  List<FailureInformationDto> mockGridFailureInformationDtos() {
        List<FailureInformationDto> retList = new LinkedList<>();
        retList.add( mockFailureInformationDto() );
        retList.add( mockFailureInformationDto() );
        retList.get(1).setBranch("W");
        //retList.get(1).setStatusExtern("IN_WORK");
        retList.get(1).setStatusIntern("IN_WORK");
        retList.get(1).setResponsibility("Lionel Lümmelprinz");
        return retList;
    }
    public static Page<FailureInformationDto> mockGridFailureInformationDtoPage() {
        List<FailureInformationDto> dtos = mockGridFailureInformationDtos();
        return new PageImpl<>(dtos, Pageable.unpaged(), dtos.size() );
    }

    public static BranchDto mockBranchDto() {
        BranchDto branchDto = new BranchDto();
        branchDto.setUuid(UUID.fromString("14d4327c-594b-11ea-82b4-0242ac130003"));
        branchDto.setName("Handel");
        branchDto.setColorCode("#3ADF00");
        return branchDto;
    }

    public static List<BranchDto> mockBranchDtoList() {
        List<BranchDto> branchDtoList = new ArrayList<>();

        BranchDto branchDto1 = mockBranchDto();
        BranchDto branchDto2 = new BranchDto();
        branchDto2.setUuid(UUID.fromString("f033c6d4-594b-11ea-8e2d-0242ac130003"));
        branchDto2.setName("Diagnostik");
        branchDto2.setColorCode("#D358F7");

        branchDtoList.add(branchDto1);
        branchDtoList.add(branchDto2);

        return branchDtoList;
    }

    public static RefBranch mockRefBranch() {
        RefBranch refBranch = new RefBranch();
        refBranch.setUuid(UUID.fromString("319fe7ae-594e-11ea-82b4-0242ac130003"));
        refBranch.setName("Pharma");
        refBranch.setColorCode("#29088A");
        return refBranch;
    }

    public static RefBranch mockRefBranch2() {
        RefBranch refBranch = new RefBranch();
        refBranch.setUuid(UUID.fromString("8fc26f00-7410-11ea-bc55-0242ac130003"));
        refBranch.setName("Textil");
        refBranch.setColorCode("#30308A");
        return refBranch;
    }

    public static List<RefBranch> mockRefBranchList() {
        List<RefBranch> refBranchList = new ArrayList<>();

        RefBranch refBranch1 = mockRefBranch();
        RefBranch refBranch2 = new RefBranch();
        refBranch2.setUuid(UUID.fromString("d03814c2-594e-11ea-8e2d-0242ac130003"));
        refBranch2.setName("Maschinenbau");
        refBranch2.setColorCode("#FF8000");

        refBranchList.add(refBranch1);
        refBranchList.add(refBranch2);

        return refBranchList;
    }

    public static FailureClassificationDto mockFailureClassificationDto() {
        FailureClassificationDto failureClassificationDto1 = new FailureClassificationDto();

        failureClassificationDto1.setUuid(UUID.fromString("9992aaed-8910-4116-b0c4-0855f8d3c28d"));
        failureClassificationDto1.setClassification("Störung");
        failureClassificationDto1.setDescription("Störungsbeschreibung");

        return failureClassificationDto1;
    }
    public static FailureClassificationDto mockFailureClassificationDto2() {
        FailureClassificationDto failureClassificationDto2 = new FailureClassificationDto();

        failureClassificationDto2.setUuid(UUID.fromString("999880c4-3127-47d5-aaee-5f778462ab0c"));
        failureClassificationDto2.setClassification("geplante Maßnahme");
        failureClassificationDto2.setDescription("Maßnahme, die geplant ist");

        return failureClassificationDto2;
    }

    public static List<FailureClassificationDto> mockFailureClassificationDtoList() {
        FailureClassificationDto failureClassificationDto1 = mockFailureClassificationDto();
        FailureClassificationDto failureClassificationDto2 = mockFailureClassificationDto2();

        List<FailureClassificationDto> failureClassificationDtoList = new ArrayList<>();
        failureClassificationDtoList.add(failureClassificationDto1);
        failureClassificationDtoList.add(failureClassificationDto2);

        return failureClassificationDtoList;
    }

    public static RefFailureClassification mockRefFailureClassification() {
        RefFailureClassification refFailureClassification1 = new RefFailureClassification();

        refFailureClassification1.setUuid(UUID.fromString("8882aaed-8910-4116-b0c4-0855f8d3c28d"));
        refFailureClassification1.setClassification("Störung");
        refFailureClassification1.setDescription(null);

        return refFailureClassification1;
    }

    public static RefFailureClassification mockRefFailureClassification2() {
        RefFailureClassification refFailureClassification2 = new RefFailureClassification();

        refFailureClassification2.setUuid(UUID.fromString("888880c4-3127-47d5-aaee-5f778462ab0c"));
        refFailureClassification2.setClassification("Ereignis");
        refFailureClassification2.setDescription("Ereignisbeschreibung");

        return refFailureClassification2;
    }
    public static List<RefFailureClassification> mockRefFailureClassificationList() {
        RefFailureClassification refFailureClassification1 = mockRefFailureClassification();
        RefFailureClassification refFailureClassification2 = mockRefFailureClassification2();

        List<RefFailureClassification> refFailureClassificationList = new ArrayList<>();
        refFailureClassificationList.add(refFailureClassification1);
        refFailureClassificationList.add(refFailureClassification2);

        return refFailureClassificationList;
    }

    public static StatusDto mockStatusDto(String status, UUID uuid) {
        StatusDto statusDto = new StatusDto();
        statusDto.setUuid(uuid);
        statusDto.setStatus(status);

        return statusDto;
    }

    public static List<StatusDto> mockStatusDtoList() {
        List<StatusDto> statusDtoList = new ArrayList<>();

        StatusDto statusDto1 = mockStatusDto("neu" , UUID.randomUUID());
        StatusDto statusDto2 = new StatusDto();
        statusDto2.setUuid(UUID.fromString("51d3c6d4-594b-11ea-8e2d-0242ac130003"));
        statusDto2.setStatus("aktiv");

        statusDtoList.add(statusDto1);
        statusDtoList.add(statusDto2);

        return statusDtoList;
    }

    public static RefStatus mockRefStatusNew() {
        RefStatus refStatus = new RefStatus();
        refStatus.setUuid(UUID.randomUUID());
        refStatus.setId(GfiProcessState.NEW.getStatusValue());
        refStatus.setStatus("neu");

        return refStatus;
    }

    public static RefStatus mockRefStatusPlanned() {
        RefStatus refStatus = new RefStatus();
        refStatus.setUuid(UUID.randomUUID());
        refStatus.setId(GfiProcessState.PLANNED.getStatusValue());
        refStatus.setStatus("geplant");

        return refStatus;
    }

    public static RefStatus mockRefStatusCreated() {
        RefStatus refStatus = new RefStatus();
        refStatus.setUuid(UUID.randomUUID());
        refStatus.setId(GfiProcessState.CREATED.getStatusValue());
        refStatus.setStatus("angelegt");

        return refStatus;
    }

    public static RefStatus mockRefStatusCANCELED() {
        RefStatus refStatus = new RefStatus();
        refStatus.setUuid(UUID.randomUUID());
        refStatus.setId(GfiProcessState.CANCELED.getStatusValue());
        refStatus.setStatus("storniert");

        return refStatus;
    }

    public static RefStatus mockRefStatusQUALIFIED() {
        RefStatus refStatus = new RefStatus();
        refStatus.setUuid(UUID.randomUUID());
        refStatus.setId(GfiProcessState.QUALIFIED.getStatusValue());
        refStatus.setStatus("qualifiziert");

        return refStatus;
    }

    public static RefStatus mockRefStatusUPDATED() {
        RefStatus refStatus = new RefStatus();
        refStatus.setUuid(UUID.randomUUID());
        refStatus.setId(GfiProcessState.UPDATED.getStatusValue());
        refStatus.setStatus("aktualisiert");

        return refStatus;
    }

    public static RefStatus mockRefStatusCOMPLETED() {
        RefStatus refStatus = new RefStatus();
        refStatus.setUuid(UUID.randomUUID());
        refStatus.setId(GfiProcessState.COMPLETED.getStatusValue());
        refStatus.setStatus("abgeschlossen");

        return refStatus;
    }

    public static RefStatus mockRefStatus2() {
        RefStatus refStatus = new RefStatus();
        refStatus.setId(12L);
        refStatus.setUuid(UUID.fromString("4a7c2640-74ae-11ea-bc55-0242ac130003"));
        refStatus.setStatus("neu");

        return refStatus;
    }

    public static List<RefStatus> mockRefStatusList() {
        List<RefStatus> refStatusList = new ArrayList<>();

        RefStatus refStatus1 = mockRefStatusCreated();
        RefStatus refStatus2 = new RefStatus();
        refStatus2.setUuid(UUID.fromString("113814c2-594e-11ea-8e2d-0242ac130003"));
        refStatus2.setStatus("geschlossen");

        refStatusList.add(refStatus1);
        refStatusList.add(refStatus2);

        return refStatusList;
    }

    public static HtblFailureInformation mockHistTblFailureInformation() {
        HtblFailureInformation obj = new HtblFailureInformation();
        obj.setId(789L);
        obj.setUuid(UUID.randomUUID());
        obj.setVersionNumber(1L);
        obj.setResponsibility("Dudley Dursley");
        obj.setVoltageLevel(Constants.VOLTAGE_LEVEL_MS);
        obj.setPressureLevel(Constants.PRESSURE_LEVEL_HD);
        obj.setFailureBegin(new java.util.Date(Date.valueOf("2022-12-01").getTime()));
        obj.setFailureEndPlanned(new java.util.Date(Date.valueOf("2022-12-02").getTime()));
        obj.setFailureEndResupplied(new java.util.Date(Date.valueOf("2022-12-03").getTime()));

        obj.setStationId("224488-123bcd");
        obj.setStationDescription("E-werk 4 Trafo 1");
        obj.setStationCoords("121,8855");
        obj.setLongitude(BigDecimal.valueOf(8.646280));
        obj.setLatitude(BigDecimal.valueOf(50.115618));

        obj.setRefRadius(mockRefRadius());

        obj.setPublicationStatus("nicht veröffentlicht");
        obj.setPublicationFreetext("Kabel kaputt");
        obj.setRefExpectedReason(mockRefExpectedReason());

        obj.setCreateDate(new java.util.Date(Date.valueOf("2020-05-08").getTime()));
        obj.setCreateUser("weizenkeimk");
        obj.setModDate(new java.util.Date(Date.valueOf("2020-05-23").getTime()));
        obj.setModUser("schlonzh");

        obj.setRefStatusIntern(mockRefStatusCreated());
        obj.setRefFailureClassification(mockRefFailureClassification());
        obj.setRefBranch(mockRefBranch());

        return obj;
    }

    public static HtblFailureInformation mockHistTblFailureInformation2() {
        HtblFailureInformation obj = new HtblFailureInformation();
        obj.setId(789L);
        obj.setUuid(UUID.randomUUID());
        obj.setVersionNumber(2L);
        obj.setResponsibility("Donald Duck");
        obj.setVoltageLevel(Constants.VOLTAGE_LEVEL_MS);
        obj.setPressureLevel(Constants.PRESSURE_LEVEL_HD);
        obj.setFailureBegin(new java.util.Date(Date.valueOf("2022-12-01").getTime()));
        obj.setFailureEndPlanned(new java.util.Date(Date.valueOf("2022-12-02").getTime()));
        obj.setFailureEndResupplied(new java.util.Date(Date.valueOf("2022-12-03").getTime()));

        obj.setStationId("567az-443bcd");
        obj.setStationDescription("Pumpwerk 13");
        obj.setStationCoords("121,8855");
        obj.setLongitude(BigDecimal.valueOf(8.846280));
        obj.setLatitude(BigDecimal.valueOf(51.115618));

        obj.setRefRadius(mockRefRadius());

        obj.setPublicationStatus("veröffentlicht");
        obj.setPublicationFreetext("Rohrbruch");
        obj.setRefExpectedReason(mockRefExpectedReason());
        obj.setCreateDate(new java.util.Date(Date.valueOf("2020-05-08").getTime()));
        obj.setCreateUser("Kleverk");
        obj.setModDate(new java.util.Date(Date.valueOf("2020-05-23").getTime()));
        obj.setModUser("Gansg");

        obj.setRefStatusIntern(mockRefStatusCreated());
        obj.setRefFailureClassification(mockRefFailureClassification());
        obj.setRefBranch(mockRefBranch());

        return obj;
    }

    public static  List<HtblFailureInformation> mockHistTblFailureInformationList() {
        List<HtblFailureInformation> retList = new LinkedList<>();
        retList.add( mockHistTblFailureInformation() );
        retList.add( mockHistTblFailureInformation2() );
        return retList;
    }

    public static FailureInformationDto mockHistFailureInformationDto() {
        FailureInformationDto dto = new FailureInformationDto();
        dto.setUuid(UUID.randomUUID());
        dto.setVersionNumber(1L);
        dto.setResponsibility("Dudley Dursley");
        dto.setVoltageLevel(Constants.VOLTAGE_LEVEL_MS);
        dto.setPressureLevel(Constants.PRESSURE_LEVEL_HD);
        dto.setFailureBegin(new java.util.Date(Date.valueOf("2022-12-01").getTime()));
        dto.setFailureEndPlanned(new java.util.Date(Date.valueOf("2022-12-02").getTime()));
        dto.setFailureEndResupplied(new java.util.Date(Date.valueOf("2022-12-03").getTime()));


        dto.setStationId("123123-123zzz");
        dto.setStationDescription("Umspannwerk 5");
        dto.setStationCoords("121,8855");
        dto.setLongitude(BigDecimal.valueOf(8.646280));
        dto.setLatitude(BigDecimal.valueOf(49.119618));

        dto.setRadiusId(UUID.randomUUID());
        dto.setRadius(500L);

        dto.setPublicationStatus(" nicht veröffentlicht");
        dto.setPublicationFreetext("Leitung abgerissen");

        dto.setExpectedReasonId(UUID.randomUUID());
        dto.setExpectedReasonText("Kabelfehler Hochspannung");

        dto.setFailureClassificationId(UUID.randomUUID());
        dto.setFailureClassification("FailClazz");

        dto.setFailureTypeId(UUID.randomUUID());
        dto.setFailureType("FailTypo");

        dto.setStatusInternId(UUID.randomUUID());
        dto.setStatusIntern("NEW");

        //dto.setStatusExtern("CLOSED");

        dto.setBranchId(UUID.randomUUID());
        dto.setBranch("G");
        dto.setBranchColorCode("#fdea64");

        dto.setCreateDate(new java.util.Date(Date.valueOf("2020-05-08").getTime()));
        dto.setCreateUser("weizenkeimk");
        dto.setModDate(new java.util.Date(Date.valueOf("2020-05-23").getTime()));
        dto.setModUser("schlonzh");

        return dto;
    }

    public static FailureInformationDto mockHistFailureInformationDto2() {
        FailureInformationDto dto = new FailureInformationDto();
        dto.setUuid(UUID.randomUUID());
        dto.setVersionNumber(2L);
        dto.setResponsibility("Paulchen Panther");
        dto.setVoltageLevel(Constants.VOLTAGE_LEVEL_MS);
        dto.setPressureLevel(Constants.PRESSURE_LEVEL_HD);
        dto.setFailureBegin(new java.util.Date(Date.valueOf("2022-12-01").getTime()));
        dto.setFailureEndPlanned(new java.util.Date(Date.valueOf("2022-12-02").getTime()));
        dto.setFailureEndResupplied(new java.util.Date(Date.valueOf("2022-12-03").getTime()));

        dto.setStationId("985236-999bcd");
        dto.setStationDescription("Trafo 135");
        dto.setStationCoords("121,8855");
        dto.setLongitude(BigDecimal.valueOf(9.646280));
        dto.setLatitude(BigDecimal.valueOf(52.115618));

        dto.setRadiusId(UUID.randomUUID());
        dto.setRadius(150L);

        dto.setPublicationStatus("veröffentlicht");
        dto.setPublicationFreetext("Kabel durchgebrochen");

        dto.setExpectedReasonId(UUID.randomUUID());
        dto.setExpectedReasonText("Kabelfehler Mittelspannung");

        dto.setFailureClassificationId(UUID.randomUUID());
        dto.setFailureClassification("FailClazz");

        dto.setFailureTypeId(UUID.randomUUID());
        dto.setFailureType("FailTypo");

        dto.setStatusInternId(UUID.randomUUID());
        dto.setStatusIntern("NEW");

//        dto.setStatusExternId(UUID.randomUUID());
//        dto.setStatusExtern("CLOSED");

        dto.setBranchId(UUID.randomUUID());
        dto.setBranch("G");
        dto.setBranchColorCode("#fdea64");

        dto.setCreateDate(new java.util.Date(Date.valueOf("2020-05-08").getTime()));
        dto.setCreateUser("weizenkeimk");
        dto.setModDate(new java.util.Date(Date.valueOf("2020-05-23").getTime()));
        dto.setModUser("schlonzh");

        return dto;
    }

    public static  List<FailureInformationDto> mockHistGridFailureInformationDtoList() {
        List<FailureInformationDto> retList = new LinkedList<>();
        retList.add( mockHistFailureInformationDto() );
        retList.add( mockHistFailureInformationDto2() );
        return retList;
    }

    public static RadiusDto mockRadiusDto() {
        RadiusDto radiusDto1 = new RadiusDto();

        radiusDto1.setUuid(UUID.randomUUID());
        radiusDto1.setRadius(300L);

        return radiusDto1;
    }

    public static RadiusDto mockRadiusDto2() {
        RadiusDto radiusDto2 = new RadiusDto();

        radiusDto2.setUuid(UUID.randomUUID());
        radiusDto2.setRadius(1555L);

        return radiusDto2;
    }

    public static List<RadiusDto> mockRadiusDtoList() {
        RadiusDto radiusDto1 = mockRadiusDto();
        RadiusDto radiusDto2 = mockRadiusDto2();

        List<RadiusDto> radiusDtoList = new ArrayList<>();
        radiusDtoList.add(radiusDto1);
        radiusDtoList.add(radiusDto2);

        return radiusDtoList;
    }

    public static RefRadius mockRefRadius() {
        RefRadius refRadius1 = new RefRadius();

        refRadius1.setUuid(UUID.randomUUID());
        refRadius1.setRadius(400L);

        return refRadius1;
    }

    public static RefRadius mockRefRadius2() {
        RefRadius refRadius2 = new RefRadius();

        refRadius2.setUuid(UUID.randomUUID());
        refRadius2.setRadius(400L);

        return refRadius2;
    }

    public static List<RefRadius> mockRefRadiusList() {
        RefRadius refRadius1 = mockRefRadius();
        RefRadius refRadius2 = mockRefRadius2();

        List<RefRadius> refRadiusList = new ArrayList<>();
        refRadiusList.add(refRadius1);
        refRadiusList.add(refRadius2);

        return refRadiusList;
    }


    public static ExpectedReasonDto mockExpectedReasonDto() {
        ExpectedReasonDto expectedReasonDto1 = new ExpectedReasonDto();

        expectedReasonDto1.setUuid(UUID.fromString("cd22ff10-6cde-11ea-bc55-0242ac130003"));
        expectedReasonDto1.setDescription("Beschreibung Rohrbruch");
        expectedReasonDto1.setDescription("Rohrbruch");
        expectedReasonDto1.setBranches("S");


        return expectedReasonDto1;
    }

    public static ExpectedReasonDto mockExpectedReasonDto2() {
        ExpectedReasonDto expectedReasonDto2 = new ExpectedReasonDto();

        expectedReasonDto2.setUuid(UUID.fromString("cd23015e-6cde-11ea-bc55-0242ac130003"));
        expectedReasonDto2.setDescription("Beschreibung Alles kaputt!");
        expectedReasonDto2.setText("Alles kaputt!");
        expectedReasonDto2.setBranches("S,G,W");

        return expectedReasonDto2;
    }

    public static List<ExpectedReasonDto> mockExpectedReasonDtoList() {
        ExpectedReasonDto expectedReasonDto1 = mockExpectedReasonDto();
        ExpectedReasonDto expectedReasonDto2 = mockExpectedReasonDto2();

        List<ExpectedReasonDto> expectedReasonDtoList = new ArrayList<>();
        expectedReasonDtoList.add(expectedReasonDto1);
        expectedReasonDtoList.add(expectedReasonDto2);

        return expectedReasonDtoList;
    }

    public static RefExpectedReason mockRefExpectedReason() {
        RefExpectedReason refExpectedReason1 = new RefExpectedReason();

        refExpectedReason1.setUuid(UUID.fromString("cd230532-6cde-11ea-bc55-0242ac130003"));
        refExpectedReason1.setDescription("Beschreibung Kabelfehler Hochspannung");
        refExpectedReason1.setText("Kabelfehler Hochspannung");
        refExpectedReason1.setBranches("W");

        return refExpectedReason1;
    }

    public static RefExpectedReason mockRefExpectedReason2() {
        RefExpectedReason refExpectedReason2 = new RefExpectedReason();

        refExpectedReason2.setUuid(UUID.fromString("cd23062c-6cde-11ea-bc55-0242ac130003"));
        refExpectedReason2.setDescription("Beschreibung Rohrleitung beschädigt");
        refExpectedReason2.setText("Rohrleitung beschädigt");
        refExpectedReason2.setBranches("S,G,W,TK");

        return refExpectedReason2;
    }

    public static List<RefExpectedReason> mockRefExpectedReasonList() {
        RefExpectedReason refExpectedReason1 = mockRefExpectedReason();
        RefExpectedReason refExpectedReason2 = mockRefExpectedReason2();

        List<RefExpectedReason> refExpectedReasonList = new ArrayList<>();
        refExpectedReasonList.add(refExpectedReason1);
        refExpectedReasonList.add(refExpectedReason2);

        return refExpectedReasonList;
    }
    public static ImportDataDto mockImportDataDto() {
        ImportDataDto importDataDto1 = new ImportDataDto();

        importDataDto1.setUuid(UUID.fromString("355b4beb-9b17-4247-bb8b-36bd01b48f9a"));
        importDataDto1.setMetaId("StoeDE_10.03.2020 10:31:000 XYZ");
        importDataDto1.setDescription("Gasleck im Haus");
        importDataDto1.setSource("Stoerungen.de");
        importDataDto1.setMessageContent("{ \"Quelle\": \"Stoerungen.de\", \"Eigenschaften\": {\"Branch\": \"Gas\", \"Ort\": \"Worms\", \"Datum\": \"2020-03-10T10:31.000Z\"}}");

        return importDataDto1;
    }
    public static ImportDataDto mockImportDataDto2() {
        ImportDataDto importDataDto2 = new ImportDataDto();

        importDataDto2.setUuid(UUID.fromString("26c6d361-96a0-41cc-bda1-4e85ad16f21a"));
        importDataDto2.setMetaId("StoeDE_14.03.2020 05:55:500 XYZ");
        importDataDto2.setDescription("Stromausfall");
        importDataDto2.setSource("Stoerungen.de");
        importDataDto2.setMessageContent("{ \"Quelle\": \"Stoerungen.de\", \"Eigenschaften\": {\"Branch\": \"Strom\", \"Ort\": \"Kassel\", \"Datum\": \"2020-06-11T23:45.000Z\"}}");

        return importDataDto2;
    }

    public static List<ImportDataDto> mockImportDataDtoList() {
        ImportDataDto importDataDto1 = mockImportDataDto();
        ImportDataDto importDataDto2 = mockImportDataDto2();

        List<ImportDataDto> importDataDtoList = new ArrayList<>();
        importDataDtoList.add(importDataDto1);
        importDataDtoList.add(importDataDto2);

        return importDataDtoList;
    }

    public static TblImportData mockTblImportData() {
        TblImportData importData1 = new TblImportData();

        importData1.setUuid(UUID.fromString("44a2aaed-8910-4116-b0c4-0855f8d3c28d"));
        importData1.setMetaId("StoeDE_10.03.2020 10:31:000 XYZ");
        importData1.setDescription("Gasleck im Haus");
        importData1.setSource("Stoerungen.de");
        importData1.setMessageContent("{ \"Quelle\": \"Stoerungen.de\", \"Eigenschaften\": {\"Branch\": \"Gas\", \"Ort\": \"Worms\", \"Datum\": \"2020-03-10T10:31.000Z\"}}");


        return importData1;
    }

    public static TblImportData mockTblImportData2() {
        TblImportData importData2 = new TblImportData();

        importData2.setUuid(UUID.fromString("26c6d361-96a0-41cc-bda1-4e85ad16f21a"));
        importData2.setMetaId("StoeDE_14.03.2020 05:55:500 XYZ");
        importData2.setDescription("Stromausfall");
        importData2.setSource("Stoerungen.de");
        importData2.setMessageContent("{ \"Quelle\": \"Stoerungen.de\", \"Eigenschaften\": {\"Branch\": \"Strom\", \"Ort\": \"Kassel\", \"Datum\": \"2020-06-11T23:45.000Z\"}}");

        return importData2;
    }

    public static List<TblImportData> mockTblImportDataList() {
        TblImportData importData1 = mockTblImportData();
        TblImportData importData2 = mockTblImportData2();

        List<TblImportData> importDataList = new ArrayList<>();
        importDataList.add(importData1);
        importDataList.add(importData2);

        return importDataList;
    }

    public static List<UUID> mockUuidList(){
        List<UUID> list = new ArrayList();

        list.add(UUID.randomUUID());
        list.add(UUID.randomUUID());
        list.add(UUID.randomUUID());

        return list;
    }

    public static List<TblFailureInformation> mockTblFailureInformationList(){
        List<TblFailureInformation> list = new ArrayList();

        list.add(mockTblFailureInformation());
        list.add(mockTblFailureInformation());
        list.add(mockTblFailureInformation());
        list.add(mockTblFailureInformation());

        return list;
    }

    public static List<TblFailureInformation> mockTblFailureInformationList2(){
        List<TblFailureInformation> list = new ArrayList();

        list.add(mockTblFailureInformation2());
        list.add(mockTblFailureInformation2());
        list.add(mockTblFailureInformation2());

        return list;
    }


    public static List<String> mockPostCodes() {
        List list = new ArrayList();

        list.add("71111");
        list.add("71122");

        return list;
    }

    public static List<String> mockStringList() {
        List list = new ArrayList();

        list.add("test1");
        list.add("test2");

        return list;
    }

    public static List<TblAddress> mockAddressList() {
        List<TblAddress> addressList = new ArrayList<>();

        TblAddress address1 = new TblAddress();
        address1.setUuid(UUID.randomUUID());
        address1.setStreet("stree1");
        address1.setPostcode("23443");
        address1.setHousenumber("44");
        address1.setLongitude(BigDecimal.valueOf(443443));

        TblAddress address2 = new TblAddress();
        address2.setUuid(UUID.randomUUID());
        address2.setStreet("stree1");
        address2.setPostcode("45465");
        address2.setHousenumber("93g");
        address2.setLongitude(BigDecimal.valueOf(546765));

        addressList.add(address1);
        addressList.add(address2);
        return addressList;
    }


    public static List<AddressDto> mockAddressDtoList() {
        List<AddressDto> addressList = new ArrayList<>();

        AddressDto address1 = new AddressDto();
        address1.setUuid(UUID.randomUUID());
        address1.setStreet("stree1");
        address1.setPostcode("23443");
        address1.setHousenumber("44");
        address1.setLongitude(BigDecimal.valueOf(443443));

        AddressDto address2 = new AddressDto();
        address2.setUuid(UUID.randomUUID());
        address2.setStreet("stree1");
        address2.setPostcode("45465");
        address2.setHousenumber("93g");
        address2.setLongitude(BigDecimal.valueOf(546765));

        addressList.add(address1);
        addressList.add(address2);
        return addressList;
    }

    public static List<HousenumberUuidDto> mockHousnumberUuidList() {
        List<HousenumberUuidDto> housenumberUuidDtoList = new ArrayList<>();

        HousenumberUuidDto housenumberUuidDto1 = new HousenumberUuidDto();
        housenumberUuidDto1.setHousenumber("44");
        housenumberUuidDto1.setUuid(UUID.randomUUID());

        HousenumberUuidDto housenumberUuidDto2 = new HousenumberUuidDto();
        housenumberUuidDto2.setHousenumber("93g");
        housenumberUuidDto2.setUuid(UUID.randomUUID());

        housenumberUuidDtoList.add(housenumberUuidDto1);
        housenumberUuidDtoList.add(housenumberUuidDto2);
        return housenumberUuidDtoList;
    }

    public static TblStation mockTblStation(){

        TblStation tblStation = new TblStation();
        tblStation.setId(224466L);
        tblStation.setUuid(UUID.randomUUID());
        tblStation.setSdox1(new BigDecimal(100.888));
        tblStation.setSdoy1(new BigDecimal(-1.888));
        tblStation.setG3efid(4L);
        tblStation.setStationId("123456");
        tblStation.setStationName("Am Waldgrund");
        tblStation.setLongitude(new BigDecimal(49.123456));
        tblStation.setLatitude(new BigDecimal(2.333333));

        return tblStation;
    }

    public static TblStation mockTblStation2(){

        TblStation tblStation = new TblStation();
        tblStation.setId(335577L);
        tblStation.setUuid(UUID.randomUUID());
        tblStation.setSdox1(new BigDecimal(44.111));
        tblStation.setSdoy1(new BigDecimal(9.777));
        tblStation.setG3efid(3456L);
        tblStation.setStationId("454545");
        tblStation.setStationName("Kleine Strasse 58");
        tblStation.setLongitude(new BigDecimal(48.987444));
        tblStation.setLatitude(new BigDecimal(12.121212));

        return tblStation;
    }

    public static TblStation mockTblStation3(){

        TblStation tblStation = new TblStation();
        tblStation.setId(323232L);
        tblStation.setUuid(UUID.randomUUID());
        tblStation.setSdox1(new BigDecimal(44.111));
        tblStation.setSdoy1(new BigDecimal(9.777));
        tblStation.setG3efid(3456L);
        tblStation.setStationId("464646");
        tblStation.setStationName("Mittlere Strasse 58");
        tblStation.setLongitude(new BigDecimal(48.987444));
        tblStation.setLatitude(new BigDecimal(12.121212));

        return tblStation;
    }

    public static List<TblStation> mockTblStationList(){

        List<TblStation> stationList = new LinkedList();
        stationList.add(mockTblStation());
        stationList.add(mockTblStation2());
        return  stationList;
    }

    public static StationDto mockStationDto(){

        StationDto stationDto = new StationDto();
        stationDto.setUuid(UUID.randomUUID());
        stationDto.setSdox1(new BigDecimal(100.888));
        stationDto.setSdoy1(new BigDecimal(-1.888));
        stationDto.setG3efid(8454L);
        stationDto.setStationId("3216549");
        stationDto.setStationName("Am Dorfrand 88");
        stationDto.setLongitude(new BigDecimal(48.556622));
        stationDto.setLatitude(new BigDecimal(8.777777));

        return stationDto;
    }

    public static StationDto mockStationDto2(){

        StationDto stationDto = new StationDto();
        stationDto.setUuid(UUID.randomUUID());
        stationDto.setSdox1(new BigDecimal(44.111));
        stationDto.setSdoy1(new BigDecimal(9.777));
        stationDto.setG3efid(3456L);
        stationDto.setStationId("454545");
        stationDto.setStationName("Lummerländer Weg 14");
        stationDto.setLongitude(new BigDecimal(45.874123));
        stationDto.setLatitude(new BigDecimal(34.343434));

        return stationDto;
    }

    public static List<StationDto> mockStationDtoList() {
        List<StationDto> stationDtoList = new ArrayList<>();
        stationDtoList.add(mockStationDto());
        stationDtoList.add(mockStationDto2());

        return  stationDtoList;
    }

    public static TblAddress mockTblAddress(){
        BigDecimal bdLatitude = new BigDecimal("52.000000");
        bdLatitude = bdLatitude.setScale(6, RoundingMode.UP);
        BigDecimal bdLongitude = new BigDecimal("1.222222");
        bdLongitude = bdLongitude.setScale(6, RoundingMode.UP);

        TblAddress tblAddress = new TblAddress();
        tblAddress.setId(1L);
        tblAddress.setUuid(UUID.randomUUID());
        tblAddress.setSdox1(new BigDecimal("2.555522"));
        tblAddress.setSdoy1(new BigDecimal("4.888992"));
        tblAddress.setG3efid(8L);
        tblAddress.setPostcode("556677");
        tblAddress.setCommunity("Oberkrämer ");
        tblAddress.setDistrict("Oberhavel");
        tblAddress.setStreet("Am alten Kanal");
        tblAddress.setHousenumber("11");
        tblAddress.setWaterConnection(true);
        tblAddress.setWaterGroup("C");
        tblAddress.setGasConnection(false);
        tblAddress.setGasGroup("B");
        tblAddress.setPowerConnection(true);
        tblAddress.setStationId("898989");
        tblAddress.setLatitude(bdLatitude);
        tblAddress.setLongitude(bdLongitude);
        tblAddress.setTelecommConnection(true);
        tblAddress.setDistrictheatingConnection(true);

        return tblAddress;
    }

    public static List<TblAddress> mockTblAddressList(){
        List<TblAddress> addressList = new ArrayList<>();
        TblAddress tblAddress1 = mockTblAddress();

        BigDecimal bdLatitude = new BigDecimal("53.000000");
        bdLatitude = bdLatitude.setScale(6, RoundingMode.UP);

        BigDecimal bdLongitude = new BigDecimal("2.222222");
        bdLongitude = bdLongitude.setScale(6, RoundingMode.UP);

        TblAddress tblAddress2 = new TblAddress();
        tblAddress2.setUuid(UUID.randomUUID());
        tblAddress2.setSdox1(new BigDecimal("2.999999"));
        tblAddress2.setSdoy1(new BigDecimal("5.777777"));
        tblAddress2.setG3efid(9L);
        tblAddress2.setPostcode("777333");
        tblAddress2.setCommunity("Bärenklau ");
        tblAddress2.setDistrict("Unterhavel");
        tblAddress2.setStreet("An der Havel");
        tblAddress2.setHousenumber("12");
        tblAddress2.setWaterConnection(false );
        tblAddress2.setWaterGroup("A");
        tblAddress2.setGasConnection(true);
        tblAddress2.setGasGroup("C");
        tblAddress2.setPowerConnection(true);
        tblAddress2.setStationId("787878");
        tblAddress2.setLatitude(bdLatitude);
        tblAddress2.setLongitude(bdLongitude);

        BigDecimal bdLatitude3 = new BigDecimal(53.555555);
        bdLatitude3.setScale(6,0);

        BigDecimal bdLongitude3 = new BigDecimal(2.666666);
        bdLongitude3.setScale(6,0);

        TblAddress tblAddress3 = new TblAddress();
        tblAddress3.setUuid(UUID.randomUUID());
        tblAddress3.setSdox1(new BigDecimal(2.999999));
        tblAddress3.setSdoy1(new BigDecimal(5.777777));
        tblAddress3.setG3efid(9L);
        tblAddress3.setPostcode("777333");
        tblAddress3.setCommunity("Bärenklau ");
        tblAddress3.setDistrict("Unterhavel");
        tblAddress3.setStreet("An der Havel");
        tblAddress3.setHousenumber("12");
        tblAddress3.setWaterConnection(false );
        tblAddress3.setWaterGroup("A");
        tblAddress3.setGasConnection(true);
        tblAddress3.setGasGroup("C");
        tblAddress3.setPowerConnection(true);
        tblAddress3.setStationId("787878");
        tblAddress3.setLatitude(bdLatitude3);
        tblAddress3.setLongitude(bdLongitude3);

        BigDecimal bdLatitude4 = new BigDecimal(53.456789);
        bdLatitude4.setScale(6,0);

        BigDecimal bdLongitude4 = new BigDecimal(2.123456);
        bdLongitude4.setScale(6,0);

        TblAddress tblAddress4 = new TblAddress();
        tblAddress4.setUuid(UUID.randomUUID());
        tblAddress4.setSdox1(new BigDecimal(2.999999));
        tblAddress4.setSdoy1(new BigDecimal(5.777777));
        tblAddress4.setG3efid(9L);
        tblAddress4.setPostcode("777333");
        tblAddress4.setCommunity("Bärenklau ");
        tblAddress4.setDistrict("Unterhavel");
        tblAddress4.setStreet("An der Havel");
        tblAddress4.setHousenumber("12");
        tblAddress4.setWaterConnection(false );
        tblAddress4.setWaterGroup("A");
        tblAddress4.setGasConnection(true);
        tblAddress4.setGasGroup("C");
        tblAddress4.setPowerConnection(true);
        tblAddress4.setStationId("787878");
        tblAddress4.setLatitude(bdLatitude4);
        tblAddress4.setLongitude(bdLongitude4);

        addressList.add(tblAddress1);
        addressList.add(tblAddress2);
        addressList.add(tblAddress3);
        addressList.add(tblAddress4);

        return addressList;
    }

    public static TblDistributionGroup mockTblDistributionGroup(){

        TblDistributionGroup tblDistributionGroup = new TblDistributionGroup();
        tblDistributionGroup.setId(1L);
        tblDistributionGroup.setUuid(UUID.randomUUID());
        tblDistributionGroup.setName("Testverteiler - Abteilung intern");
        tblDistributionGroup.setDistributionTextPublish("Liebe Kollegen blabla ...");
        tblDistributionGroup.setEmailSubjectPublish("Betreff Test");

        return tblDistributionGroup;
    }

    public static TblDistributionGroup mockTblDistributionGroup2(){

        TblDistributionGroup tblDistributionGroup = new TblDistributionGroup();
        tblDistributionGroup.setId(2L);
        tblDistributionGroup.setUuid(UUID.randomUUID());
        tblDistributionGroup.setName("Testverteiler - Gruppe intern");
        tblDistributionGroup.setDistributionTextPublish("Liebste Kollegen blabla ...");

        return tblDistributionGroup;
    }

    public static List<TblDistributionGroup> mockDistributionGroupList() {
        List<TblDistributionGroup> distributionGroupList = new ArrayList<>();

        distributionGroupList.add(mockTblDistributionGroup());
        distributionGroupList.add(mockTblDistributionGroup2());
        return distributionGroupList;
    }


    public static List<TblFailureInformationDistributionGroup> mockFailureInformationDistributionGroupList() {
        List<TblDistributionGroup> distributionGroupList = new ArrayList<>();

        distributionGroupList.add(mockTblDistributionGroup());
        distributionGroupList.add(mockTblDistributionGroup2());
        return distributionGroupList.stream()
                .map( x -> {
                    TblFailureInformationDistributionGroup newRel = new TblFailureInformationDistributionGroup();
                    newRel.setFailureInformation( mockTblFailureInformation());
                    newRel.setDistributionGroup(x);
                    return newRel; })
                .collect(Collectors.toList());
    }


    public static DistributionGroupDto mockDistributionGroupDto(){

        DistributionGroupDto distributionGroupDto = new DistributionGroupDto();
        distributionGroupDto.setUuid(UUID.randomUUID());
        distributionGroupDto.setName("Testverteiler - Abteilung intern");
        distributionGroupDto.setDistributionTextPublish("Liebe Kollegen blabla ...");
        distributionGroupDto.setEmailSubjectPublish("TestBetreff");
        return distributionGroupDto;
    }


    public static DistributionGroupDto mockDistributionGroupDto2(){

        DistributionGroupDto distributionGroupDto = new DistributionGroupDto();
        distributionGroupDto.setUuid(UUID.randomUUID());
        distributionGroupDto.setName("Testverteiler - Kunden extern");
        distributionGroupDto.setDistributionTextPublish("Sehr geehrte Damen und Herren blabla ...");

        return distributionGroupDto;
    }

    public static  List<DistributionGroupDto> mockDistributionGroupDtoList() {
        List<DistributionGroupDto> distributionGroupDtoList = new ArrayList<>();
        distributionGroupDtoList.add(mockDistributionGroupDto());
        distributionGroupDtoList.add(mockDistributionGroupDto2());
        return distributionGroupDtoList;
    }


    public static TblDistributionGroupMember mockTblDistributionGroupMember(){

        TblDistributionGroupMember tblDistributionGroupMember = new TblDistributionGroupMember();
        tblDistributionGroupMember.setUuid(UUID.randomUUID());
        tblDistributionGroupMember.setTblDistributionGroup(mockTblDistributionGroup());
        tblDistributionGroupMember.setContactId(UUID.randomUUID());
        tblDistributionGroupMember.setPostcodes("12345,54321");

        return tblDistributionGroupMember;
    }

    public static TblDistributionGroupMember mockTblDistributionGroupMember2(){

        TblDistributionGroupMember tblDistributionGroupMember = new TblDistributionGroupMember();
        tblDistributionGroupMember.setUuid(UUID.randomUUID());
        tblDistributionGroupMember.setTblDistributionGroup(mockTblDistributionGroup());
        tblDistributionGroupMember.setContactId(UUID.randomUUID());

        return tblDistributionGroupMember;
    }

    public static List<TblDistributionGroupMember> mockDistributionGroupMemberList() {
        List<TblDistributionGroupMember> distributionGroupMemberList = new ArrayList<>();

        distributionGroupMemberList.add(mockTblDistributionGroupMember());
        distributionGroupMemberList.add(mockTblDistributionGroupMember2());
        return distributionGroupMemberList;
    }


    public static DistributionGroupMemberDto mockDistributionGroupMemberDto(){

        DistributionGroupMemberDto distributionGroupMemberDto = new DistributionGroupMemberDto();
        distributionGroupMemberDto.setUuid(UUID.randomUUID());
        distributionGroupMemberDto.setDistributionGroupUuid(mockDistributionGroupDto().getUuid());
        distributionGroupMemberDto.setDistributionGroup(mockDistributionGroupDto().getName());
        distributionGroupMemberDto.setContactId(UUID.randomUUID());
        distributionGroupMemberDto.setEmail("test@test.de");
        distributionGroupMemberDto.setMobileNumber("01823777102334");
        List<String> postcodeList = new ArrayList<>();
        postcodeList.add("12345");
        postcodeList.add("54321");
        distributionGroupMemberDto.setPostcodeList(postcodeList);

        return distributionGroupMemberDto;
    }


    public static DistributionGroupMemberDto mockDistributionGroupMemberDto2(){

        DistributionGroupMemberDto distributionGroupMemberDto = new DistributionGroupMemberDto();
        distributionGroupMemberDto.setUuid(UUID.randomUUID());
        distributionGroupMemberDto.setDistributionGroupUuid(mockDistributionGroupDto().getUuid());
        distributionGroupMemberDto.setDistributionGroup(mockDistributionGroupDto().getName());
        distributionGroupMemberDto.setContactId(UUID.randomUUID());
        distributionGroupMemberDto.setMobileNumber("01723777102334");
        distributionGroupMemberDto.setEmail("test2@test.de");

        return distributionGroupMemberDto;
    }

    public static  List<DistributionGroupMemberDto> mockDistributionGroupMemberDtoList() {
        List<DistributionGroupMemberDto> distributionGroupMemberDtoList = new ArrayList<>();
        distributionGroupMemberDtoList.add(mockDistributionGroupMemberDto());
        distributionGroupMemberDtoList.add(mockDistributionGroupMemberDto2());
        return distributionGroupMemberDtoList;
    }

    public static Map<String, String> mockRabbitMqchannelTypeToNameMap() {
        return mockRabbitMqchannelNameToTypeMap().entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }

    public static Map<String, String> mockRabbitMqchannelNameToTypeMap() {
        Map<String,String> channelNameToTypeMap = new HashMap<>();
        channelNameToTypeMap.put("Mail (lang)",Constants.CHANNEL_TPYE_LONG_MAIL);
        channelNameToTypeMap.put("Mail (kurz)",Constants.CHANNEL_TPYE_SHORT_MAIL);
        channelNameToTypeMap.put("stoerungsauskunftde",Constants.CHANNEL_TPYE_STOERUNGSAUSLKUNFT_DE);
        channelNameToTypeMap.put("App und Internet",Constants.CHANNEL_TPYE_PUBLICATION_OWNDMZ);
        return channelNameToTypeMap;
    }
    public static RabbitMqChannel mockRabbitMqChannelMailLong() {
        RabbitMqChannel rChannel = new RabbitMqChannel();
        rChannel.setName("Mail (lang)");
        rChannel.setExportKey("mockmail_export_key");
        rChannel.setExportQueue("mockmail_export_queue");
        rChannel.setType(Constants.CHANNEL_TPYE_LONG_MAIL);
        return rChannel;
    }

    public static RabbitMqChannel mockRabbitMqChannelMailShort() {
        RabbitMqChannel rChannel = new RabbitMqChannel();
        rChannel.setName("Mail (kurz)");
        rChannel.setExportKey("mockmail_export_key");
        rChannel.setExportQueue("mockmail_export_queue");
        rChannel.setType(Constants.CHANNEL_TPYE_SHORT_MAIL);
        return rChannel;
    }

    public static RabbitMqChannel mockRabbitMqChannelWebcomponent(){
        RabbitMqChannel rChannel = new RabbitMqChannel();
        rChannel.setName("App und Internet");
        rChannel.setExportKey("mockmail_export_key");
        rChannel.setExportQueue("mockmail_export_queue");
        rChannel.setType(Constants.CHANNEL_TPYE_PUBLICATION_OWNDMZ);
        return rChannel;
    }

    public static RabbitMqChannel mockRabbitMqChannelStoerungsauskunft(){
        RabbitMqChannel rChannel = new RabbitMqChannel();
        rChannel.setName("Störungsauskunft.de");
        rChannel.setExportKey("mocks_export_key");
        rChannel.setExportQueue("mockst_export_queue");
        rChannel.setType(Constants.CHANNEL_TPYE_STOERUNGSAUSLKUNFT_DE);
        return rChannel;
    }


    public static TblFailinfoStation mockTblFailureInformationStation() {
        TblFailinfoStation tblFailureInformationStation = new TblFailinfoStation();
        tblFailureInformationStation.setStationStationId("23456");
        tblFailureInformationStation.setFailureInformation(mockTblFailureInformation());
        tblFailureInformationStation.setId(299L);
        return tblFailureInformationStation;
    }

    public static List<TblFailinfoStation> mockTblFailureInformationStationList(){
        TblFailinfoStation mock1 =  mockTblFailureInformationStation();
        TblFailinfoStation mock2 =  mockTblFailureInformationStation();
        TblFailinfoStation mock3 =  mockTblFailureInformationStation();
        TblFailinfoStation mock4 =  mockTblFailureInformationStation();

        List<TblFailinfoStation> list = Arrays.asList(mock1, mock2, mock3, mock4);
        return list;
    }

    public static  FailureInformationStationDto mockFailureInformationStationDto() {
        FailureInformationStationDto failureInfoGroupDto = new FailureInformationStationDto();
        failureInfoGroupDto.setFailureInformationId(13579L);
        failureInfoGroupDto.setStationStationId("97531");
        return failureInfoGroupDto;
    }

    public static  TblFailureInformationDistributionGroup mockTblFailureInformationDistributionGroup() {
        TblFailureInformationDistributionGroup tblFailureInformationDistributionGroup = new TblFailureInformationDistributionGroup();
        tblFailureInformationDistributionGroup.setDistributionGroup(mockTblDistributionGroup()); // 5L
        tblFailureInformationDistributionGroup.setFailureInformation(mockTblFailureInformation()); // 8L
        tblFailureInformationDistributionGroup.setId(99L);
        return tblFailureInformationDistributionGroup;
    }


    public static  FailureInformationDistributionGroupDto mockFailureInformationDistributionGroupDto() {
        FailureInformationDistributionGroupDto failureInfoGroupDto = new FailureInformationDistributionGroupDto();
        failureInfoGroupDto.setFailureInformationId(5L);
        failureInfoGroupDto.setDistributionGroupId(8L);
        return failureInfoGroupDto;
    }

    public static List<String> mockPublicationChannelList(){
        List<String> publicationChannels= new LinkedList<String>();
        publicationChannels.add("MOCKMAIL");
        publicationChannels.add("MOCKSMS");
        publicationChannels.add("MOCKXY");

        return publicationChannels;
    }

    public static TblFailureInformationPublicationChannel mockTblFailureInformationPublicationChannel(){
        TblFailureInformationPublicationChannel tfipChannel = new TblFailureInformationPublicationChannel();
        tfipChannel.setId(1L);
        tfipChannel.setTblFailureInformation(mockTblFailureInformation());
        tfipChannel.setPublicationChannel("MOCKMAIL");
        tfipChannel.setPublished(true);
        return tfipChannel;
    }

    public static TblFailureInformationPublicationChannel mockTblFailureInformationPublicationChannel2(){
        TblFailureInformationPublicationChannel tfipChannel = new TblFailureInformationPublicationChannel();
        tfipChannel.setId(1L);
        tfipChannel.setTblFailureInformation(mockTblFailureInformation());
        tfipChannel.setPublicationChannel("MOCKSMS");
        tfipChannel.setPublished(true);
        return tfipChannel;
    }

    public static FailureInformationPublicationChannelDto mockFailureInformationPublicationChannelDto(){
        FailureInformationPublicationChannelDto fipChannelDto = new FailureInformationPublicationChannelDto();
        fipChannelDto.setFailureInformationId(UUID.randomUUID());
        fipChannelDto.setPublicationChannel("MOCKMAIL");
        fipChannelDto.setPublished(false);
        return fipChannelDto;
    }

    public static FailureInformationPublicationChannelDto mockFailureInformationPublicationChannelDto2(){
        FailureInformationPublicationChannelDto fipChannelDto = new FailureInformationPublicationChannelDto();
        fipChannelDto.setFailureInformationId(UUID.randomUUID());
        fipChannelDto.setPublicationChannel("MOCKCHANNEL");
        fipChannelDto.setPublished(true);
        return fipChannelDto;
    }

    public static List<TblFailureInformationPublicationChannel> mockTblFailureInformationPublicationChannelList(){
        List<TblFailureInformationPublicationChannel> channelList = new LinkedList<>();
        channelList.add(mockTblFailureInformationPublicationChannel());
        channelList.add(mockTblFailureInformationPublicationChannel2());

        return channelList;
    }

    public static List<FailureInformationPublicationChannelDto> mockFailureInformationPublicationChanneDtolList(){
        List<FailureInformationPublicationChannelDto> channelList = new LinkedList<>();
        channelList.add(mockFailureInformationPublicationChannelDto());
        channelList.add(mockFailureInformationPublicationChannelDto2());

        return channelList;
    }

    public static List<StationPolygonDto> mockPolygonStationCoordinatesList(){
        List<StationPolygonDto> stationPolygonDtoList = new ArrayList<>();

        StationPolygonDto stationPolygonDto1 = new StationPolygonDto();
        StationPolygonDto stationPolygonDto2 = new StationPolygonDto();

        List<List<BigDecimal>> polygonCoordinatesList = mockPolygonCoordinatesList();
        stationPolygonDto1.setPolygonCoordinatesList(polygonCoordinatesList);
        stationPolygonDto2.setPolygonCoordinatesList(polygonCoordinatesList);

        stationPolygonDtoList.add(stationPolygonDto1);
        stationPolygonDtoList.add(stationPolygonDto2);

        return stationPolygonDtoList;
    }

    public static List<List<BigDecimal>> mockPolygonCoordinatesList(){

        List<List<BigDecimal>> cordinatesList = new LinkedList<>();

        List<BigDecimal> firstCoordinate = new ArrayList<>();
        firstCoordinate.add(new BigDecimal("53.5"));
        firstCoordinate.add(new BigDecimal("2.7"));

        List<BigDecimal> secondCoordinate = new ArrayList<>();
        secondCoordinate.add(new BigDecimal("52.8"));
        secondCoordinate.add(new BigDecimal("2.1"));

        List<BigDecimal> thirdCoordinate = new ArrayList<>();
        thirdCoordinate.add(new BigDecimal("53.66"));
        thirdCoordinate.add(new BigDecimal("2.33"));

        List<BigDecimal> fourthCoordinate = new ArrayList<>();
        fourthCoordinate.add(new BigDecimal("52.9"));
        fourthCoordinate.add(new BigDecimal("2.0"));

        List<BigDecimal> fifthCoordinate = new ArrayList<>();
        fifthCoordinate.add(new BigDecimal("53.45"));
        fifthCoordinate.add(new BigDecimal("2.77"));

        cordinatesList.add(firstCoordinate);
        cordinatesList.add(secondCoordinate);
        cordinatesList.add(thirdCoordinate);
        cordinatesList.add(fourthCoordinate);
        cordinatesList.add(fifthCoordinate);

        return cordinatesList;
    }

    public static TblFailureInformationReminderMailSent mockTblFailureInformationReminderMailSent(){
        TblFailureInformationReminderMailSent tblFiReminderMailSent = new TblFailureInformationReminderMailSent();
        tblFiReminderMailSent.setId(1L);
        tblFiReminderMailSent.setTblFailureInformation(mockTblFailureInformation());
        tblFiReminderMailSent.setMailSent(true);
        tblFiReminderMailSent.setDateMailSent(new java.util.Date(Date.valueOf("2020-05-08").getTime()));
        return tblFiReminderMailSent;
    }
    public static TblFailureInformationReminderMailSent mockTblFailureInformationReminderMailSentFuture(){
        TblFailureInformationReminderMailSent tblFiReminderMailSent = new TblFailureInformationReminderMailSent();
        // add one day to currentDate to get a date in the future
        var currentDate = new java.util.Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        var futureDate = calendar.getTime();
        tblFiReminderMailSent.setId(1L);
        tblFiReminderMailSent.setTblFailureInformation(mockTblFailureInformation());
        tblFiReminderMailSent.setMailSent(true);
        tblFiReminderMailSent.setDateMailSent(futureDate);
        return tblFiReminderMailSent;
    }

    public static  HtblFailureInformationStation mockHistTblFailureInformationStation() {
        HtblFailureInformationStation histTblFailureInfoStation = new HtblFailureInformationStation();
        histTblFailureInfoStation.setHid(299L);
        histTblFailureInfoStation.setFkTblFailureInformation(42L);
        histTblFailureInfoStation.setVersionNumber(3L);
        histTblFailureInfoStation.setStationStationId("23456");
        return histTblFailureInfoStation;
    }

    public static  HtblFailureInformationStation mockHistTblFailureInformationStation2() {
        HtblFailureInformationStation histTblFailureInfoStation = new HtblFailureInformationStation();
        histTblFailureInfoStation.setHid(106L);
        histTblFailureInfoStation.setFkTblFailureInformation(42L);
        histTblFailureInfoStation.setVersionNumber(3L);
        histTblFailureInfoStation.setStationStationId("44556");
        return histTblFailureInfoStation;
    }

    public static List<HtblFailureInformationStation>  mockHistTblFailureInformationStationList() {
        List<HtblFailureInformationStation> htblFailureInformationStationList = new ArrayList<>();
        htblFailureInformationStationList.add(mockHistTblFailureInformationStation());
        htblFailureInformationStationList.add(mockHistTblFailureInformationStation2());
        return htblFailureInformationStationList;
    }

    public static  HistFailureInformationStationDto mocHistkFailureInformationStationDto() {
        HistFailureInformationStationDto histFailureInfoGroupDto = new HistFailureInformationStationDto();
        histFailureInfoGroupDto.setFailureInformationId(13579L);
        histFailureInfoGroupDto.setVersionNumber(7L);
        histFailureInfoGroupDto.setStationStationId("97531");
        return histFailureInfoGroupDto;
    }

    public static List<TblFailureInformationDistributionGroup> mockTblFailureInformationDistributionGroupList(){
        TblFailureInformationDistributionGroup ds1 = mockTblFailureInformationDistributionGroup();
        TblFailureInformationDistributionGroup ds2 = mockTblFailureInformationDistributionGroup();
        TblFailureInformationDistributionGroup ds3 = mockTblFailureInformationDistributionGroup();

        List<TblFailureInformationDistributionGroup> list =  new ArrayList();
        list.add(ds1);
        list.add(ds2);
        list.add(ds3);

        return list;
    }

    public static ResponseEntity<Resource> mockResponseEntity() {
        byte[] binData = "LariFariHuhumann".getBytes(StandardCharsets.UTF_8);
        final ResponseEntity.BodyBuilder responseBuilder = ResponseEntity
                .status(HttpStatus.OK)
                .contentLength(binData.length)
                .contentType(MediaType.valueOf("text/csv"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"lumpi.csv\"");

        return responseBuilder.body(new ByteArrayResource(binData));
    }

    public static FailureInformationLastModDto mockFailureInformationLastModDto() {
        FailureInformationLastModDto dto = new FailureInformationLastModDto();
        dto.setUuid(UUID.randomUUID());
        dto.setLastModification(new java.util.Date());
        return dto;
    }

    public static RabbitMqMessageDto mockMailMessageDto() {
        RabbitMqMessageDto mailMessageDto = new RabbitMqMessageDto();
        FailureInformationDto failureInformationDto = new FailureInformationDto();
        mailMessageDto.setFailureInformationDto(failureInformationDto);
        mailMessageDto.setBody("Subject: BetreffzeileTest Body: EmailText Content Test");
        mailMessageDto.setDistributionGroup("MailDistributionGroupTest");
        mailMessageDto.setEmailSubject("Test Betreff");

        List<String> mailAddressList = new ArrayList<>();
        mailAddressList.add("tester@tester.net");
        mailMessageDto.setMailAddresses(mailAddressList);

        return mailMessageDto;
    }

    public static RabbitMqMessageDto mockMailMessageDtoWrongRecipientFormat() {
        RabbitMqMessageDto mailMessageDto = new RabbitMqMessageDto();
        FailureInformationDto failureInformationDto = new FailureInformationDto();
        mailMessageDto.setFailureInformationDto(failureInformationDto);
        mailMessageDto.setBody("Subject: BetreffzeileTest Body: EmailText Content Test");
        mailMessageDto.setDistributionGroup("MailDistributionGroupTest");

        List<String> mailAddressList = new ArrayList<>();
        mailAddressList.add("testertester.net");
        mailMessageDto.setMailAddresses(mailAddressList);

        return mailMessageDto;
    }
}
