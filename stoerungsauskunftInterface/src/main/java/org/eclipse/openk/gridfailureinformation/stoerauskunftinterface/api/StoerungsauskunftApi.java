/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api;

import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.StoerungsauskunftOutage;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.StoerungsauskunftUserNotification;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Profile({"!test"})
@FeignClient(name="Stoerauskunft.de", url= "${stoerungsauskunft.apiUrl}")
public interface StoerungsauskunftApi {
    //Example call: "https://stage-api-operator.stoerungsauskunft.de/api/v1.0/outage?additiveMode=false"
    @PostMapping(value= "/outage")
    feign.Response postOutage(@RequestBody List<StoerungsauskunftOutage> outage,
                              @RequestParam("additiveMode") boolean additiveMode);

    @GetMapping(value= "/reports")
    List<StoerungsauskunftUserNotification> getUserNotification(@RequestParam(value="sectorType") Integer sectorType);
}
