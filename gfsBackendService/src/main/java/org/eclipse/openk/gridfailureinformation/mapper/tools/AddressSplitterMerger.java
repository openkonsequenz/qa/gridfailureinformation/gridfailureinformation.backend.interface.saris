/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mapper.tools;

import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;

import java.util.Optional;

public class AddressSplitterMerger {
    private AddressSplitterMerger() {}

    public static void splitFreetextAddress(String srcAddressType, String srcCity, String srcDistrict, String srcPostcode,
                                            FailureInformationDto failureInformationDto){
        if( Constants.FREETEXT_ADDRESS_TYPE.equalsIgnoreCase(srcAddressType)) {
            failureInformationDto.setFreetextCity(srcCity);
            failureInformationDto.setFreetextDistrict(srcDistrict);
            failureInformationDto.setFreetextPostcode(srcPostcode);
            failureInformationDto.setCity(null);
            failureInformationDto.setDistrict(null);
            failureInformationDto.setPostcode(null);
            failureInformationDto.setStreet(null);
            failureInformationDto.setHousenumber(null);
        }
    }

    public static void mergeFreetextAddress(FailureInformationDto source, TblFailureInformation destTbl){

        if( isFreetextPresent(source) ) { // freetextAddress is present
            destTbl.setAddressType(Constants.FREETEXT_ADDRESS_TYPE);
            destTbl.setCity(source.getFreetextCity());
            destTbl.setDistrict(source.getFreetextDistrict());
            destTbl.setPostcode(source.getFreetextPostcode());
        }
        else {
            destTbl.setAddressType(null);
        }
    }

    public static void mergeFreetextAddress(FailureInformationDto source, HtblFailureInformation destTbl){

        if( isFreetextPresent(source) ) { // freetextAddress is present
            destTbl.setAddressType(Constants.FREETEXT_ADDRESS_TYPE);
            destTbl.setCity(source.getFreetextCity());
            destTbl.setDistrict(source.getFreetextDistrict());
            destTbl.setPostcode(source.getFreetextPostcode());
        }
        else {
            destTbl.setAddressType(null);
        }
    }

    private static boolean isFreetextPresent(FailureInformationDto source) {
        StringBuilder sb = new StringBuilder();
        sb.append(Optional.ofNullable(source.getFreetextCity()).orElse(""));
        sb.append(Optional.ofNullable(source.getFreetextDistrict()).orElse(""));
        sb.append(Optional.ofNullable(source.getFreetextPostcode()).orElse(""));
        return !sb.toString().trim().isEmpty();
    }
}
