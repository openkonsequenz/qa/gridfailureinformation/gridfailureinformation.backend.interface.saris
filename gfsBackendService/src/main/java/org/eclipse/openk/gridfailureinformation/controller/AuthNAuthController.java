/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.AuthNAuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Log4j2
@RestController
public class AuthNAuthController {
    private final AuthNAuthService authNAuthService;

    public AuthNAuthController(AuthNAuthService authNAuthService) {
        this.authNAuthService = authNAuthService;
    }

    @Operation(summary = "Logout vom Auth-und-Auth-Modul")
    @ApiResponses(value = {@ApiResponse(responseCode = "204", description = "Erfolgreich durchgeführt")})
    @GetMapping("/logout")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public ResponseEntity<?> logout() {
        authNAuthService.logout();
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Test für GetDirectMeasureLink")
    @ApiResponses(value = {@ApiResponse(responseCode = "204", description = "Erfolgreich durchgeführt")})
    @GetMapping("/test-directmeasurelink/{uuid}")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public ResponseEntity<String> testGetDirectMeasureLink(@PathVariable UUID uuid) {
        String directMeasureLink = authNAuthService.getDirectMeasureLink(uuid.toString());
        return ResponseEntity.ok(directMeasureLink);
    }
}
