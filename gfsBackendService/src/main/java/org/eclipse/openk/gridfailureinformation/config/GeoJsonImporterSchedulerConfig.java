/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.config;

import org.eclipse.openk.gridfailureinformation.service.ImportGeoJsonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


@Configuration
@EnableScheduling
@ConditionalOnProperty(prefix = "geoJsonImport", name = "enabled", havingValue = "true", matchIfMissing = false)
public class GeoJsonImporterSchedulerConfig {
    private static final Logger log = LoggerFactory.getLogger("geojsonlogger");
    private static final String SCHEDULER_NAME = "GeoJsonImporter-Scheduler";

    private final ImportGeoJsonService importGeoJsonService;

    public GeoJsonImporterSchedulerConfig(ImportGeoJsonService importGeoJsonService) {
        this.importGeoJsonService = importGeoJsonService;
    }

    @Scheduled(cron = "${geoJsonImport.cron}")
    public void importGeoJsonTask() {
        log.debug("Executing" + SCHEDULER_NAME + " task: Importing GeoJsons" );
        importGeoJsonService.importGeoJsonFile();
        log.debug("Finished " + SCHEDULER_NAME + " task: Importing GeoJsons");

    }
}
