package org.eclipse.openk.gridfailureinformation.sarisinterface.config;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.sarisinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.sarisinterface.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Log4j2
@Configuration
@EnableScheduling
@ConditionalOnProperty(prefix = "saris.scheduling-import", name = "enabled", havingValue = "true", matchIfMissing = false)
public class ImportSchedulerConfig {
    @Value("${saris.scheduling-import.cron}")
    private String cronExpression;

    private static final String SCHEDULER_NAME = "SARIS-Import-Scheduler";

    private final ImportService importService;

    public ImportSchedulerConfig(ImportService importService) {
        this.importService = importService;
        log.info(SCHEDULER_NAME + " is enabled with cron expression: " + cronExpression);
    }

    @Scheduled(cron = "${saris.scheduling-import.cron}")
    public void scheduleTaskImportMessages() {
        log.info("Executing" + SCHEDULER_NAME + " task: Importing available messages from SARIS");
        importService.importForeignFailures(Constants.SARIS_ELECTRICITY_BRANCH_ID, false, false);
        importService.importForeignFailures(Constants.SARIS_WATER_BRANCH_ID, false, false);
        importService.importForeignFailures(Constants.SARIS_GAS_BRANCH_ID, false, false);
        log.info("Finished " + SCHEDULER_NAME + " task: Importing available messages from SARIS");
    }
}
