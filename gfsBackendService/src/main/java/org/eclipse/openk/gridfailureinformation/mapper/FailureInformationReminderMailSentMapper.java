/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mapper;

import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationReminderMailSent;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationReminderMailSentDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FailureInformationReminderMailSentMapper {
    @Mappings({
        @Mapping(source = "tblFailureInformation.uuid", target = "failureInformationId"),
})
    FailureInformationReminderMailSentDto toFailureInformationReminderDto(TblFailureInformationReminderMailSent tblFailureInformationReminderMailSent);
}
