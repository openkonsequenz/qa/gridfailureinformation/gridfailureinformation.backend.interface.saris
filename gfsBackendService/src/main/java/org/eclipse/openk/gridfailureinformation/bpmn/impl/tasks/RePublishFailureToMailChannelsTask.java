/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.ServiceTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessSubject;
import org.eclipse.openk.gridfailureinformation.constants.Constants;

@Log4j2
public class RePublishFailureToMailChannelsTask extends ServiceTask<GfiProcessSubject> {

    private final GfiProcessState processState;

    public RePublishFailureToMailChannelsTask(GfiProcessState processState) {
        super("Meldung an Kanäle veröffentlichen");
        this.processState = processState;
    }

    @Override
    protected void onLeaveStep(GfiProcessSubject subject) {
        if (Constants.PUB_STATUS_VEROEFFENTLICHT.equalsIgnoreCase(subject.getFailureInformationDto().getPublicationStatus())){
            subject.getProcessHelper().publishFailureInformation( subject.getFailureInformationDto(), true , processState );
        }
        log.debug("PublishFailureToChannelsTask onLeaveStep");
    }
}
