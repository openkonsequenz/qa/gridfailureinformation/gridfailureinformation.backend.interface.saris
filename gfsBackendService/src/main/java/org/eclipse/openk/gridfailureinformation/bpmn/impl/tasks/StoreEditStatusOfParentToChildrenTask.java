/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.ServiceTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessSubject;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;

import java.util.List;

@Log4j2
public class StoreEditStatusOfParentToChildrenTask extends ServiceTask<GfiProcessSubject> {
    public StoreEditStatusOfParentToChildrenTask() {
        super("Bearbeitungsstatus der übergeordneten Meldung für die untergeordneten setzen");
    }

    @Override
    protected void onLeaveStep(GfiProcessSubject subject) {
        FailureInformationDto parent = subject.getFailureInformationDto();
        ProcessHelper processHelper = subject.getProcessHelper();

        List<FailureInformationDto> children = processHelper.getSubordinatedChildren(
                parent );
        GfiProcessState parentState = (GfiProcessState) processHelper.getProcessStateFromStatusUuid(
                parent.getStatusInternId() );


        children.forEach(x -> processHelper.storeEditStatus(x, parentState));

        log.debug("StoreEditStatusOfParentToChildrenTask onLeaveStep");
    }
}
